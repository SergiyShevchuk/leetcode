package Common.DataStructures;
import java.util.List;

public class BinnaryNode {
    public int val;
    public List<BinnaryNode> children;

    public BinnaryNode() {}

    public BinnaryNode(int _val) {
        val = _val;
    }

    public BinnaryNode(int _val, List<BinnaryNode> _children) {
        val = _val;
        children = _children;
    }
}
