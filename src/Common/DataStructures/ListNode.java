package Common.DataStructures;

public class ListNode {
    public int val;
    public ListNode next;
    public ListNode prev;
    public ListNode(int x) { val = x; }

    public static ListNode simpleListOne() {
        ListNode one = new ListNode(1);
        ListNode two = new ListNode(4);
        ListNode fourth = new ListNode(5);
        one.next = two;
        two.next = fourth;

        return one;
    }

    public static ListNode simpleListTwo() {
        ListNode one = new ListNode(1);
        ListNode two = new ListNode(3);
        ListNode fourth = new ListNode(4);

        one.next = two;
        two.next = fourth;

        return one;
    }

    public static ListNode simpleListThree() {
        ListNode one = new ListNode(2);
        ListNode two = new ListNode(6);

        one.next = two;

        return one;
    }

    public static ListNode palindromeList() {
        ListNode one = new ListNode(1);
        ListNode two = new ListNode(2);
        ListNode three = new ListNode(3);
        ListNode fourth = new ListNode(4);
        ListNode fourth2 = new ListNode(4);
        ListNode five = new ListNode(3);
        ListNode six = new ListNode(2);
        ListNode seven = new ListNode(1);


        one.next = two;
        two.next = three;
//        three.next = fourth;
//        fourth.next = fourth2;
//        fourth2.next = five;
//        five.next = six;
//        six.next = seven;

        return one;
    }
}
