package Common.DataStructures;

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int x) { val = x; }

    public static TreeNode basicTree() {
        TreeNode f = new TreeNode(1);

        TreeNode fl = new TreeNode(2);
        TreeNode fr = new TreeNode(3);

        TreeNode fll = new TreeNode(4);
        TreeNode frr = new TreeNode(5);

        f.left = fl;
        f.right = fr;

        fl.left = fll;
        fl.right = frr;

        return f;
    }
}
