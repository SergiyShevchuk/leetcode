package Companies.Microsoft;

/*
 Task:
 https://leetcode.com/discuss/interview-question/365872/
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MC_001 {
    public static int solution(int[] A) {
        int retValue = -1;
        if (A == null || A.length == 0) {
            return retValue;
        }

        HashMap<Integer, ArrayList<Integer>> storage = new HashMap<>();

        for (int i: A) {
            int summ = summOfDigits(i);
            if (!storage.containsKey(summ)) {
                ArrayList<Integer> list = new ArrayList<>();
                list.add(i);
                storage.put(summ, list);
            } else {
                ArrayList<Integer> list = storage.get(summ);
                if (list.size() == 2) {
                    list.add(i);
                    Collections.sort(list);
                    list.remove(0);
                } else {
                    list.add(i);
                }

                storage.put(summ, list);
            }
        }


        for (int key: storage.keySet()) {
            ArrayList<Integer> list = storage.get(key);
            int size = list.size();
            if (size > 1) {
                retValue = Math.max(retValue, list.get(size - 2) + list.get(size - 1));
            }
        }

        return retValue;
    }

    private static int summOfDigits(int i) {
        int summ = 0;

        while (i != 0) {
            summ += i % 10;
            i = i / 10;
        }

        return summ;
    }
}
