package Companies.TestTasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TT_01 {
    public static int solution(int N) {
        if (N == 0) {
            return 5 * 10;
        }

        int sign = N < 0 ? -1 : 1;
        List<Integer> digits = getDigits(sign * N);

           for (int i = 0; i < digits.size(); i++) {
               if (digits.get(i) * sign < 5) {
                   return createNumber(digits, i) * sign;
               }
           }

        return createNumber(digits, digits.size()) * sign;
    }

    public static int createNumber(List<Integer> digits, int position) {
        digits.add(position, 5);

        int retValue = 0;
        for (int i = 0; i < digits.size(); i++) {
            retValue = retValue * 10 + digits.get(i);
        }

        return retValue;
    }

    public static List<Integer> getDigits(int N) {
        List<Integer> digits = new ArrayList<>();
        while (N > 0) {
            digits.add(N % 10);
            N = N / 10;
        }
        Collections.reverse(digits);

        return digits;
    }
}
