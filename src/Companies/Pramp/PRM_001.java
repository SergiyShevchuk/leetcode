package Companies.Pramp;

/*
Find The Duplicates
Given two sorted arrays arr1 and arr2 of passport numbers,
implement a function findDuplicates that returns an array of all
passport numbers that are both in arr1 and arr2. Note that the
output array should be sorted in an ascending order.

Let N and M be the lengths of arr1 and arr2,
respectively. Solve for two cases and analyze the time
& space complexities of your solutions: M ≈ N - the array lengths are
 approximately the same M ≫ N - arr2 is much bigger than arr1.

Example:

input:  arr1 = [1, 2, 3, 5, 6, 7], arr2 = [3, 6, 7, 8, 20]

output: [3, 6, 7]
 */

import java.util.ArrayList;

public class PRM_001 {
    public static int[] findDuplicates(int[] arr1, int[] arr2) {
        ArrayList<Integer> list = new ArrayList<>();

        int N = arr1.length;
        int M = arr2.length;

        int left = 0;
        int right = 0;

        while (N > 0 && M > 0) {
            if (arr1[left] == arr2[right]) {
                list.add(arr1[left]);
                left++;
                right++;
                N--;
                M--;
            } else if (arr1[left] < arr2[right]) {
                left++;
                N--;
            } else {
                right++;
                M--;
            }
        }

        if (M > 0) {
            for (int i = right; i < arr2.length; i++) {
                list.add(arr2[i]);
            }
        }

        return pasportsList(list);
    }

    private static int[] pasportsList(ArrayList<Integer> list) {
        int size = list.size();
        int[] storage = new int[size];

        for (int i = 0; i < size; i++) {
            storage[i] = list.get(i);
        }

        return storage;
    }
}
