package Leetcode.Medium.Graph;
import java.util.*;

public class LC_49 {
    public List<List<String>> groupAnagrams(String[] strs) {
        List<List<String>> finalList = new ArrayList<>();
        if (strs == null || strs.length == 0) {
            return finalList;
        }

        Map<List<Integer>, List<String>> map = new HashMap<>();

        for (int i = 0; i < strs.length; i++) {
            List<Integer> wordIndex = transformWord(strs[i]);
            if (map.containsKey(wordIndex)) {
                List<String> list = map.get(wordIndex);
                list.add(strs[i]);
                map.put(wordIndex, list);
            } else {
                List<String> list = new ArrayList<>();
                list.add(strs[i]);
                map.put(wordIndex, list);
            }
        }

        for (List<String> value: map.values()) {
            finalList.add(value);
        }

        return finalList;
    }

    private List<Integer> transformWord(String word) {
        int[] wordIndex = new int[26];
        for (int i = 0; i < word.length(); i++) {
            wordIndex[word.charAt(i) - 97]++;
        }
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < wordIndex.length; i++) {
            list.add(i, wordIndex[i]);
        }

        return list;
    }
}
