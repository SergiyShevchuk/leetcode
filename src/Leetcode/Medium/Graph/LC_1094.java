package Leetcode.Medium.Graph;

import java.util.ArrayList;
import java.util.List;

public class LC_1094 {

    /*
    int stops[] = new int[1001];
  for (int t[] : trips) {
      stops[t[1]] += t[0];
      stops[t[2]] -= t[0];
  }
  for (int i = 0; capacity >= 0 && i < 1001; ++i) capacity -= stops[i];
  return capacity >= 0;
     */


    public static boolean carPoolingCustom(int[][] trips, int capacity) {
        int[] stops = new int[1001];

        for (int i = 0; i < trips.length; i++) {
            stops[trips[i][1]] += trips[i][0];
            stops[trips[i][2]] -= trips[i][0];
        }

        for (int i = 0; capacity >= 0 && i < 1001; ++i) {
            capacity -= stops[i];
        }

        boolean retValue = capacity >= 0;

        return retValue;
    }


    public static boolean carPooling(int[][] trips, int capacity) {

        int lastStop = 0;

        for (int[] trip: trips) {
            lastStop = Math.max(lastStop, trip[2]);
        }

        int[] stops = new int[lastStop + 1];

        for (int[] trip: trips) {
            for (int i = trip[1]; i <= trip[2]; i++) {
                if (i == trip[2]) {
                    stops[i] -= trip[0];
                } else {
                    stops[i] += trip[0];
                }
            }
        }

        for (int busStop: stops) {
            if (busStop > capacity) {
                return false;
            }
        }


        return true;
    }
}
