package Leetcode.Medium.Graph;
import java.util.*;

public class LC_721 {

    /*
    Input:
accounts = [["John", "johnsmith@mail.com", "john00@mail.com"],
["John", "johnnybravo@mail.com"], ["John", "johnsmith@mail.com", "john_newyork@mail.com"],
["Mary", "mary@mail.com"]]

Output: [["John", 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com'],
["John", "johnnybravo@mail.com"], ["Mary", "mary@mail.com"]]
     */

    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        List<List<String>> finalList = new ArrayList<>();
        if (accounts == null || accounts.size() == 0) {
            return finalList;
        }

        Map<String, String> emailToUserID = new HashMap<>();

        return finalList;
    }
}
