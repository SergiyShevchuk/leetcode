package Leetcode.Medium.Graph;

import java.util.*;

public class LC_743 {
    /*
    There are N network nodes, labelled 1 to N.

    Given times, a list of travel times as directed edges times[i] = (u, v, w),
    where u is the source node, v is the target node, and w is the time it
    takes for a signal to travel from source to target.

    Now, we send a signal from a certain node K.
    How long will it take for all nodes to receive the signal?
    If it is impossible, return -1.

    Input: times = [[2,1,1],[2,3,1],[3,4,1]], N = 4, K = 2
    Output: 2
     */

    public static int networkDelayTime(int[][] times, int N, int K) {
        Map<Integer, List<Integer>> adjacencyList = new HashMap<>();
        Map<String, Integer> costs = new HashMap<>();

        for (int[] vertex: times) {
            List<Integer> list = adjacencyList.getOrDefault(vertex[0], new ArrayList<>());
            list.add(vertex[1]);
            String costID = String.valueOf(vertex[0]) + String.valueOf(vertex[1]);
            costs.put(costID, vertex[2]);
            adjacencyList.put(vertex[0], list);
        }

        Queue<Integer> queue = new LinkedList<>();
        queue.add(K);

        int retValue = 0;

        while (!queue.isEmpty()) {
            Integer vertexID = queue.poll();
            List<Integer> vertexes = adjacencyList.get(vertexID);
            adjacencyList.remove(vertexID);

            for (Integer nextVertex: vertexes) {
                String costID = String.valueOf(vertexID) + String.valueOf(nextVertex);
                Integer cost = costs.get(costID);
                retValue = Math.max(retValue, retValue + cost);
                if (adjacencyList.containsKey(nextVertex)) {
                    queue.add(nextVertex);
                }
            }
        }

        return retValue;
    }
}
