package Leetcode.Medium.ClassesConstructor;
import Common.DataStructures.TreeNode;

import java.lang.reflect.Array;
import java.util.*;

public class BSTIterator {

    private List<TreeNode> list;
    private int currNodeIndex;

    public BSTIterator(TreeNode root) {
        this.list = new ArrayList<>();
        this.currNodeIndex = 0;
        this.contructNodesList(root, list);
    }

    /** @return the next smallest number */
    public int next() {
        return list.get(currNodeIndex++).val;
    }

    /** @return whether we have a next smallest number */
    public boolean hasNext() {
        if (currNodeIndex >= list.size()) {
            return false;
        }

        return true;
    }

    private void contructNodesList(TreeNode root, List<TreeNode> list) {
        if (root == null) { return; }
        contructNodesList(root.left, list);
        list.add(root);
        contructNodesList(root.left, list);
    }
}
