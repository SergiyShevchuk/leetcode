package Leetcode.Medium.ClassesConstructor;
import java.util.*;

public class RandomizedSet {

    Map<Integer, Integer> storage;
    LinkedList<Integer> linkedList;

    public RandomizedSet() {
        this.storage = new HashMap<>();
        this.linkedList = new LinkedList<>();
    }

    /** Inserts a value to the set.
     * Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        if (storage.containsKey(val)) {
            return false;
        } else {
            linkedList.addLast(val);
            storage.put(val, linkedList.size() - 1);
        }

        return true;
    }

    /** Removes a value from the set.
     * Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        if (storage.containsKey(val)) {
            return false;
        } else {
            Integer index = storage.get(val);
            linkedList.remove(index);
            storage.remove(val);
        }

        return true;
    }

    /** Get a random element from the set. */
    public int getRandom() {
        Random rand = new Random();
        Integer index = rand.nextInt(linkedList.size());

        return linkedList.get(index);
    }
}
