package Leetcode.Medium.DFS_BFS;
import java.util.*;

public class LC_127 {
    public static int ladderLength(String beginWord,
                                   String endWord,
                                   List<String> wordList) {

        wordList.add(beginWord);
        Map<String, Set<String>> graph = createGraph(wordList);
        if (!graph.containsKey(endWord) || graph.get(beginWord).size() == 0) {
            return 0;
        }

        Queue<List<String>> queue = new LinkedList<>();
        queue.add(new ArrayList<>(Arrays.asList(beginWord)));
        Set<String> visited = new HashSet<>();
        int counter = 1;

        while (!queue.isEmpty()) {
            List<String> startLayer = queue.poll();
            List<String> nextLayer = new ArrayList<>();
            for (String layerVertex: startLayer) {
                visited.add(layerVertex);
                Set<String> neibors = graph.get(layerVertex);
                for (String neiborVertex: neibors) {
                    if (neiborVertex.equals(endWord)) {
                        return counter++;
                    }
                    if (!visited.contains(neiborVertex)) {
                        nextLayer.add(neiborVertex);
                    }
                }
            }
            if (nextLayer.size() > 0) {
                counter++;
                queue.add(nextLayer);
            }
        }

        return counter;
    }

    private static Map<String, Set<String>> createGraph(List<String> wordList) {
        Map<String, Set<String>> storage = new HashMap<>();

        for (String word: wordList) {
            Set<String> neibors = new HashSet<>();
            for (int i = 0; i < wordList.size(); i++) {
                String potentialNeibor = wordList.get(i);
                if (isWordsHasOnePermutatin(word, potentialNeibor) == true) {
                    neibors.add(potentialNeibor);
                }
            }
            storage.put(word, neibors);
        }

        return storage;
    }

    private static boolean isWordsHasOnePermutatin(String left, String right) {
        if (left.length() != right.length() || left.equals(right)) { return false; }

        boolean onePermutation = false;

        for (int i = 0; i < left.length(); i++) {
            if (left.charAt(i) != right.charAt(i)) {
                if (onePermutation == true) {
                    return false;
                } else {
                    onePermutation = true;
                }
            }
        }

        return true;
    }
}
