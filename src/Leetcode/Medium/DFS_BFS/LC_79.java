package Leetcode.Medium.DFS_BFS;
import java.util.*;

public class LC_79 {
    public static boolean exist(char[][] board, String word) {
        if (board.length == 0 || word == null || word.length() == 0) {
            return false;
        }

        List<int[]> startNodes = new ArrayList<>();
        char startChar = word.charAt(0);

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == startChar) {
                    startNodes.add(new int[] {i, j});
                }
            }
        }

        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        for (int i = 0; i < startNodes.size(); i++) {
            int[] startNode = startNodes.get(i);
            if (isWordExist(board,
                    word,
                    0,
                    startNode[0],
                    startNode[1],
                    directions) == true) {
                return true;
            }
        }

        return false;
    }

    private static boolean isWordExist(char[][] board,
                                String word,
                                int charIndex,
                                int i,
                                int j,
                                int[][] directions) {
        if (i < 0 ||
                i >= board.length ||
                j < 0 ||
                j >= board[i].length ||
                board[i][j] != word.charAt(charIndex)) {
            return false;
        }

        char currChar = word.charAt(charIndex);

        if (word.charAt(charIndex) == 'F') {
            System.out.println(1);
        }

        if (charIndex == word.length() - 1) {
            return true;
        }

        board[i][j] = '#';

        for (int[] direction: directions) {
            if (isWordExist(board,
                    word,
                    charIndex + 1,
                    i + direction[0],
                    j + direction[1],
                    directions) == true) {
                return true;
            }
        }

        board[i][j] = word.charAt(charIndex);

        return false;
    }
}
