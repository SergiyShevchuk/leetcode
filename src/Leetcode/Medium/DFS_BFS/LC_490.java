package Leetcode.Medium.DFS_BFS;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class LC_490 {

    public static boolean hasPathOriginal(int[][] maze,
                                  int[] start,
                                  int[] destination) {
        boolean[][] visited = new boolean[maze.length][maze[0].length];
        int[][] dirs={{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
        Queue < int[] > queue = new LinkedList < > ();
        queue.add(start);
        visited[start[0]][start[1]] = true;
        while (!queue.isEmpty()) {
            int[] s = queue.remove();
            if (s[0] == destination[0] && s[1] == destination[1])
                return true;
            for (int[] dir: dirs) {
                int x = s[0] + dir[0];
                int y = s[1] + dir[1];
                while (x >= 0 && y >= 0 && x < maze.length && y < maze[0].length && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                }
                if (!visited[x - dir[0]][y - dir[1]]) {
                    queue.add(new int[] {x - dir[0], y - dir[1]});
                    visited[x - dir[0]][y - dir[1]] = true;
                }
            }
        }
        return false;
    }


    // My solution
    public static boolean hasPath(int[][] maze, int[] start, int[] destination) {
        Queue<int[]> queue = new LinkedList<>();
        maze[start[0]][start[1]] = 1;
        queue.add(start);
        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        while (!queue.isEmpty()) {
            int[] currentMove = queue.poll();
            int x = currentMove[0];
            int y = currentMove[1];

            if (x == destination[0] && y == destination[1]) {
                return true;
            }

            for (int[] direction: directions) {
                int i = direction[0] + x;
                int j = direction[1] + y;

                while (i >= 0 && j >= 0 && i < maze.length && j < maze[0].length && maze[i][j] == 0) {
                    i += direction[0];
                    j += direction[1];
                }

                if (maze[i - direction[0]][j - direction[1]] != 1) {
                    queue.add(new int[] {i - direction[0], j - direction[1]});
                    maze[i - direction[0]][j - direction[1]] = 1;
                }

            }
        }

        return false;
    }
}
