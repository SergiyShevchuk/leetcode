package Leetcode.Medium.DFS_BFS;

public class LC_200 {

    static int[][] directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    // BFS Solution
    public static int numIslandsBFS(char[][] grid) {
        int islands = 0;

        return 0;
    }

    private static void BFSPath(int i, int j, char[][] grid) {

    }

    // DFS Solution
    public static int numIslandsDFS(char[][] grid) {

        int islands = 0;

        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return islands;
        }

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    DFSPath(i, j, grid);
                    islands++;
                }
            }
        }

        return islands;
    }

    private static void DFSPath(int i, int j, char[][] grid) {
        if (i < 0 || i >= grid.length || j < 0 || j  >= grid[i].length || grid[i][j] != '1') {
            return;
        }
        grid[i][j] = '0';
        for (int[] direction: directions) {
            DFSPath(i + direction[0], j + direction[1], grid);
        }
    }
}
