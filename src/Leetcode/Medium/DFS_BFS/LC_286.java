package Leetcode.Medium.DFS_BFS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_286 {
    public static void wallsAndGates(int[][] rooms) {
        if (rooms == null || rooms.length == 0 || rooms[0].length == 0) {
            return;
        }

        ArrayList<Queue<Integer>> startPoints = new ArrayList<>();
        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < rooms[i].length; j++) {
                if (rooms[i][j] == 0) {
                    Queue<Integer> startPoint = new LinkedList<>();
                    startPoint.add(i);
                    startPoint.add(j);
                    startPoints.add(startPoint);
                }
            }
        }

        Queue<ArrayList<Queue<Integer>>> layer = new LinkedList<>();
        layer.add(startPoints);
        int[][] directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        int counter = 0;

        while (!layer.isEmpty()) {
            ArrayList<Queue<Integer>> currentLayer = layer.poll();
            ArrayList<Queue<Integer>> nextLayer = new ArrayList<>();
            counter++;
            for (Queue<Integer> point: currentLayer) {
                int i = point.poll();
                int j = point.poll();
                for (int[] direction: directions) {
                    if (isStepAvailable(rooms,
                             i + direction[0],
                            j + direction[1],
                            counter)) {
                        Queue<Integer> nextStep = new LinkedList<>();
                        nextStep.add(i + direction[0]);
                        nextStep.add(j + direction[1]);
                        nextLayer.add(nextStep);
                    }
                }
            }
            if (nextLayer.size() != 0) {
                layer.add(nextLayer);
            }
        }

        System.out.println(rooms);
    }

    private static boolean isStepAvailable(int[][] rooms, int i, int j, int counter) {
        if (i < 0
                || i >= rooms.length
                || j < 0
                || j >= rooms[i].length
                || rooms[i][j] == -1
                || rooms[i][j] < counter) {
            return false;
        }

        rooms[i][j] = counter;

        return true;
    }
}
