package Leetcode.Medium.K_Problems;
import Common.DataStructures.TreeNode;
import java.util.*;

public class LC_863 {
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        List<Integer> list = new ArrayList<>();

        if (root == null || target == null) { return list; }
        if (K == 0) {
            list.add(target.val);
            return list;
        }

        HashMap<TreeNode, List<Set<TreeNode>>> storage = new HashMap<>();
        createAdjacencyList(root, storage, null);

        Queue<List<TreeNode>> queue = new LinkedList<>();
        List<TreeNode> startPoint = new ArrayList<>();
        startPoint.add(target);
        queue.add(startPoint);

        Set<TreeNode> visited = new HashSet<>();
        Integer counter = 0;

        while (!queue.isEmpty()) {
            List<TreeNode> currentLayer = queue.poll();
            if (K == counter) {
                for (TreeNode currNode: currentLayer) {
                    list.add(currNode.val);
                }
                return list;
            }

            List<TreeNode> nextLayer = new ArrayList<>();

            for (TreeNode currNode: currentLayer) {
                visited.add(currNode);
                    List<Set<TreeNode>> connections = storage.get(currNode);
                    for (TreeNode child: connections.get(0)) {
                        if (!visited.contains(child)) {
                            nextLayer.add(child);
                            visited.add(child);
                        }
                    }
                    for (TreeNode parent: connections.get(1)) {
                        if (!visited.contains(parent)) {
                            nextLayer.add(parent);
                            visited.add(parent);
                        }
                    }
            }

            if (nextLayer.size() != 0) {
                counter++;
                queue.add(nextLayer);
            }
        }

        return list;
    }

    private void createAdjacencyList(TreeNode root,
                                     HashMap<TreeNode, List<Set<TreeNode>>> storage,
                                     TreeNode parentalNode) {

        if (root == null) { return; }
        List<Set<TreeNode>> list = new ArrayList<>();
        Set<TreeNode> childs = new HashSet<>();
        if (root.left != null) { childs.add(root.left); }
        if (root.right != null) { childs.add(root.right); }
        list.add(childs);
        Set<TreeNode> parent = new HashSet<>();
        if (parentalNode != null) { parent.add(parentalNode); }
        list.add(parent);
        storage.put(root, list);

        createAdjacencyList(root.left, storage, root);
        createAdjacencyList(root.right, storage, root);
    }
}
