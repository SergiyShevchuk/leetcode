package Leetcode.Medium.K_Problems;

import java.util.HashSet;
import java.util.Set;

public class LC_451 {
     // "tree"

    public static String frequencySort(String s) {
        if (s == null) { return null; }
        if (s.length() <= 1) { return s; }

        char[] storage = new char[128];
        Set<Character> uniqueValues = new HashSet<>();

        for (char curr: s.toCharArray()) {
            storage[curr]++;
            uniqueValues.add(curr);
        }

        StringBuilder retValue = new StringBuilder();

        for (int i = 0; i < 128; i++) {
            constructFrequencyString(storage, retValue, uniqueValues);
            if (uniqueValues.isEmpty()) {
                break;
            }
        }

        return retValue.toString();
    }

    private static void constructFrequencyString(char[] storage,
                                                 StringBuilder string,
                                                 Set<Character> uniqueValues) {
        int currMax = 0;
        int currIndex = 0;
        for (int i = 0; i < storage.length; i++) {
            if (currMax < storage[i]) {
                currMax = storage[i];
                currIndex = i;
            }
        }

        String tmp = "";
        uniqueValues.remove(storage[currIndex]);

        for (int i = 0; i < currMax; i++) {
            tmp += Character.toString(currIndex);
        }

        storage[currIndex] = 0;

        string.append(tmp);
    }
}
