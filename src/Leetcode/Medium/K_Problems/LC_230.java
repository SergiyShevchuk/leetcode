package Leetcode.Medium.K_Problems;

import Common.DataStructures.TreeNode;

import java.util.Stack;

public class LC_230 {
    public int kthSmallest(TreeNode root, int k) {
        Stack<TreeNode> stack = new Stack<>();
        while (root != null) {
            while (root != null) {
                stack.add(root);
                root = root.left;
            }
            root = stack.pop();
            if (--k == 0) { return root.val; }
            root = root.right;
        }

        return 0;
    }
}
