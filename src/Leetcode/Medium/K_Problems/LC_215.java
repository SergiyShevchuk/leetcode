package Leetcode.Medium.K_Problems;

import java.util.PriorityQueue;

public class LC_215 {
    public static int findKthLargest(int[] nums, int k) {

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

        for (Integer value: nums) {
            priorityQueue.add(value);
            if (priorityQueue.size() > k) {
                priorityQueue.poll();
            }
        }

        int result = priorityQueue.poll();

        return result;
    }
}
