package Leetcode.Medium.K_Problems;
import java.util.*;

public class LC_692 {
    public static List<String> topKFrequent(String[] words, int k) {
        List<String> storage = new ArrayList<>();
        if (words == null || words.length == 0) {
            return storage;
        }

        Map<String, Integer> map = new HashMap<>();
        for (String word: words) {
            map.put(word, map.getOrDefault(word, 0) + 1);
        }

        PriorityQueue<String> priorityQueue = new PriorityQueue<String>(
                (w1, w2) -> map.get(w1).equals(map.get(w2)) ?
                        w2.compareTo(w1) : map.get(w1) - map.get(w2) );

        for (String key: map.keySet()) {
            priorityQueue.add(key);
            if (priorityQueue.size() > k) {
                priorityQueue.poll();
            }
        }

        while (!priorityQueue.isEmpty()) {
            storage.add(priorityQueue.poll());
        }
        Collections.reverse(storage);

        return storage;
    }
}
