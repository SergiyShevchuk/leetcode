package Leetcode.Medium.K_Problems;
import java.util.*;

// Input: nums = [1,1,1,2,2,3], k = 2
//Output: [1,2]

public class LC_347 {
    public static List<Integer> topKFrequent(int[] nums, int k) {

        Map<Integer, Integer> map = new HashMap<>();
        for (Integer value: nums) {
            if (map.containsKey(value)) {
                map.put(value, map.get(value) + 1);
            } else {
                map.put(value, 1);
            }
        }

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        for (Integer key: map.keySet()) {
            priorityQueue.add(map.get(key));
            if (priorityQueue.size() > k) {
                priorityQueue.poll();
            }
        }

        List<Integer> list = new ArrayList<>();

        while (!priorityQueue.isEmpty()) {
            Integer fValue = priorityQueue.poll();
            for (Integer key: map.keySet()) {
                if (map.get(key) == fValue) {
                    list.add(key);
                    map.remove(key);
                    break;
                }
            }
        }


        return list;
    }
}
