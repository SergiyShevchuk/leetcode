package Leetcode.Medium;

import java.util.PriorityQueue;

public class LC_215 {
    public static int findKthLargest(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        PriorityQueue<Integer> queueStorage = new PriorityQueue<>();
        for (Integer value: nums) {
            queueStorage.add(value);
        }

        for (int index = 0; index < nums.length; index++) {
            nums[index] = queueStorage.remove();
        }

        int kValue = nums[nums.length - k];

        return kValue;
    }
}

// YjBiMmI0MWQtNzhkMC00YzNkLTlmMmYtZmQwODZjMDJiM2Yy

// MmU0ZjA2YmMtMjQ4OS00MzQzLWIzNDktM2I1NTZkNjI2ZDA2