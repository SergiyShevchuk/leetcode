package Leetcode.Medium.LinkedList;

import Common.DataStructures.ListNode;

public class LC_2 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) { return null; }

        ListNode dummyHead = new ListNode(-1);
        ListNode dummyPointer = new ListNode(-2);
        dummyHead.next = dummyPointer;

        int carry = 0;

        while (l1 != null || l2 != null) {
            int leftValue = l1 != null ? l1.val : 0;
            int rigthValue = l2 != null ? l2.val : 0;

            int summ = rigthValue + leftValue + carry;
            carry = summ / 10;

            ListNode dummyNode = new ListNode(summ % 10);
            dummyPointer.next = dummyNode;
            dummyPointer = dummyPointer.next;

            if (l1 != null) { l1 = l1.next; }
            if (l2 != null) { l2 = l2.next; }
        }

        if (carry > 0) {
            ListNode dummyNode = new ListNode(carry);
            dummyPointer.next = dummyNode;
            dummyPointer = dummyPointer.next;
        }

        ListNode newHead = dummyHead.next.next;
        dummyHead.next.next = null;
        newHead = reverseList(newHead);

        return newHead;
    }

    private ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode curr = head;

        while (curr != null) {
            ListNode tmp = curr.next;
            curr.next = prev;
            prev = curr;
            curr = tmp;
        }

        return prev;
    }
}
