package Leetcode.Medium.LinkedList;

import Common.DataStructures.ListNode;
import java.util.*;

public class LC_146 {

    Map<Integer, CacheListNode> nodesTable = new HashMap<>();

    int totalValuesInCache;
    int maxCapacity;

    CacheListNode head;
    CacheListNode tail;

    public LC_146(int capacity) {
        totalValuesInCache = 0;
        maxCapacity = capacity;

        nodesTable.clear();
        head = new CacheListNode();
        tail = new CacheListNode();

        head.next = tail;
        tail.prev = head;

        List<String> aa = new ArrayList<>();

        aa.remove(0);
    }

    public int get(int key) {
        if (!nodesTable.containsKey(key)) { return -1; }

        CacheListNode curr = nodesTable.get(key);

        moveToHead(curr);
        return curr.value;
    }

    public void put(int key, int value) {
        CacheListNode node = nodesTable.get(key);

        if (node == null) {
            // Item not found, create a new entry
            CacheListNode newNode = new CacheListNode();
            newNode.key = key;
            newNode.value = value;

            // Add to the hashtable and the actual list that represents the cache
            nodesTable.put(key, newNode);
            addToFront(newNode);
            totalValuesInCache++;

            // If over capacity remove the LRU item
            if (totalValuesInCache > maxCapacity) {
                removeLRUEntry();
            }
        } else {
            // If item is found in the cache, just update it and move it to the head of the list
            node.value = value;
            moveToHead(node);
        }
    }


    // Private sections

    private void removeLRUEntry() {
        CacheListNode tail = popTail();

        nodesTable.remove(tail.key);
        --totalValuesInCache;
    }

    private CacheListNode popTail() {
        CacheListNode tailItem = tail.prev;
        removeFromList(tailItem);

        return tailItem;
    }

    private void addToFront(CacheListNode node) {
        // Wire up the new node being to be inserted
        node.prev = head;
        node.next = head.next;

      /*
        Re-wire the node after the head. Our node is still sitting "in the middle of nowhere".
        We got the new node pointing to the right things, but we need to fix up the original
        head & head's next.

        head <-> head.next <-> head.next.next <-> head.next.next.next <-> ...
        ^            ^
        |- new node -|

        That's where we are before these next 2 lines.
      */
        head.next.prev = node;
        head.next = node;
    }

    private void removeFromList(CacheListNode node) {
        CacheListNode savedPrev = node.prev;
        CacheListNode savedNext = node.next;

        savedPrev.next = savedNext;
        savedNext.prev = savedPrev;
    }

    private void moveToHead(CacheListNode node) {
        removeFromList(node);
        addToFront(node);
    }

    private class CacheListNode {
        int key;
        int value;

        CacheListNode prev;
        CacheListNode next;
    }
}
