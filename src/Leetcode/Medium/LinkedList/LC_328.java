package Leetcode.Medium.LinkedList;

import Common.DataStructures.ListNode;

// Odd - even

public class LC_328 {
    public static ListNode oddEvenList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode oddDummy = new ListNode(-100);
        ListNode oddPointer = new ListNode(-50);
        oddDummy.next = oddPointer;

        ListNode evenDummy = new ListNode(-99);
        ListNode evenPointer = new ListNode(-9);
        evenDummy.next = evenPointer;

        ListNode pointer = head;
        int counter = 1;

        while (pointer != null) {
            if (counter % 2 == 0) {
                evenPointer.next = pointer;
                evenPointer = evenPointer.next;
            } else {
                oddPointer.next = pointer;
                oddPointer = oddPointer.next;
            }
            pointer = pointer.next;
            counter++;
        }

        if (counter % 2 == 0) {
            evenPointer.next = null;
        }

        oddPointer.next = evenDummy.next.next;
        ListNode retValue = oddDummy.next.next;

        return retValue;
    }
}
