package Leetcode.Medium.LinkedList;

import Common.DataStructures.RandomListNode;

public class LC_138 {
    public RandomListNode copyRandomList(RandomListNode head) {
        if (head == null) { return head; }
        if (head.next == null && head.random == null) {
            RandomListNode copy = new RandomListNode(head.val);
            return copy;
        }

       return head;
    }
}
