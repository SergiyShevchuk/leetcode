package Leetcode.Medium.Arrays;

import java.util.HashMap;
import java.util.Map;

public class LC_525 {
    public static int findMaxLength(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int counter = 0;
        int maxValue = 0;

        for (int i = 0; i < nums.length; i++) {
            counter = counter + (nums[i] == 1 ? 1 : -1);
            if (map.containsKey(counter)) {
                maxValue = Math.max(maxValue, i - map.get(counter));
            } else {
                map.put(counter, i);
            }
        }

       return maxValue;
    }
}
