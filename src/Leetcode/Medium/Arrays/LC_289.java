package Leetcode.Medium.Arrays;

public class LC_289 {
    // Game of Life
    public void gameOfLife(int[][] board) {
        if (board.length == 0 || board[0].length == 0) {
            return;
        }

        int[][] nextState = new int[board.length][board[0].length];
        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1},
                {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                computeNextState(i, j, board, nextState, directions);
            }
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = nextState[i][j];
            }
        }
    }

    private void computeNextState(int i,
                                  int j,
                                  int[][] board,
                                  int[][] nextState,
                                  int[][] directions) {
        int aliveNeibors = 0;
        for (int[] direction: directions) {
            if (isNeiborALive(i + direction[0],
                    j + direction[1],
                    board)) {
                aliveNeibors++;
            }
        }
        if (board[i][j] == 0 && aliveNeibors == 3) {
            nextState[i][j] = 1;
        } else if (board[i][j] == 1 && (aliveNeibors >= 2 && aliveNeibors <= 3)) {
            nextState[i][j] = 1;
        }
    }

    private boolean isNeiborALive(int i, int j, int[][] board) {
        if (i < 0 ||
                i >= board.length ||
                j < 0 ||
                j >= board[i].length ||
                board[i][j] == 0) {
            return false;
        }

        return true;
    }

}
