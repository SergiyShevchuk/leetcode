package Leetcode.Medium.Arrays;

import java.util.Arrays;

public class LC_253 {
    public static int minMeetingRooms(int[][] intervals) {
        if (intervals.length == 0 || intervals[0].length == 0) { return 0; }
        if (intervals.length == 1) { return 1;}

        int[] startPoints = new int[intervals.length];
        int[] endPoints = new int[intervals.length];

        for (int i = 0; i < intervals.length; i++) {
            startPoints[i] = intervals[i][0];
            endPoints[i] = intervals[i][1];
        }

        Arrays.sort(startPoints);
        Arrays.sort(endPoints);

        int rooms = 0; int endInterval = 0;

        for (int i = 0; i < startPoints.length; i++) {
            if (startPoints[i] < endPoints[endInterval]) {
                rooms++;
            } else {
                endInterval++;
            }
        }

        return rooms;
    }
}

