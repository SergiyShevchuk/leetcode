package Leetcode.Medium.Arrays;

public class LC_238 {

    //Optimal solution
    public static int[] productExceptSelf(int[] nums) {
        int summ = 1;
        int currElement = nums[0];
        for (int i = 1; i < nums.length; i++) {
            summ *= nums[i];
        }

        return nums;
    }


    //BruteForce solution
    public static int[] productExceptSelfBruteForce(int[] nums) {
        int[] newNums = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            int summ = 1;
            for (int j = 0; j < nums.length; j++) {
                if (i != j) {
                    summ *= nums[j];
                }
            }

            newNums[i] = summ;
        }

        return newNums;
    }
}
