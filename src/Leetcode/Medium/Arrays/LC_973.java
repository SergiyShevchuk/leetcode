package Leetcode.Medium.Arrays;

/* K Closest Points to Origin */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Stack;

public class LC_973 {
    public static int[][] kClosest(int[][] points, int K) {
        PriorityQueue<Integer> distances = new PriorityQueue<>();
        HashMap<Integer, Stack<ArrayList<Integer>>> storage = new HashMap<>();

        for (int[] point: points) {
            int length = distance(point[0], point[1]);
            ArrayList<Integer> visited = new ArrayList<>();
            visited.add(point[0]);
            visited.add(point[1]);
            Stack<ArrayList<Integer>> pointsStack = storage.get(length);
            if (pointsStack != null) {
                pointsStack.add(visited);
                storage.put(length, pointsStack);
            } else {
                Stack<ArrayList<Integer>> newPointsStack = new Stack<>();
                newPointsStack.add(visited);
                storage.put(length, newPointsStack);
            }
            distances.add(length);
        }

        int[][] retValue = new int[K][2];
        while (K > 0) {
            Integer coordinatesKey = distances.poll();
            Stack<ArrayList<Integer>> coordinatesStack = storage.get(coordinatesKey);
            ArrayList<Integer> coordinates = coordinatesStack.pop();
            retValue[K - 1][0] = coordinates.get(0);
            retValue[K - 1][1] = coordinates.get(1);
            if (coordinatesStack.isEmpty()) {
                storage.remove(coordinatesKey);
            } else {
                storage.put(coordinatesKey, coordinatesStack);
            }
            K--;
        }

        return retValue;
    }

    private static int distance(int i, int j) {
        return (i * i) + (j * j);
    }
}
