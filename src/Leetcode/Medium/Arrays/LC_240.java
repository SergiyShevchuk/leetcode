package Leetcode.Medium.Arrays;

public class LC_240 {
    public static boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length == 0 || matrix[0].length == 0) { return true; }
        int steps = matrix.length + matrix[0].length;

        int row = matrix.length - 1;
        int coll = 0;

        for (int i = 0; i <= steps; i++) {
            if (row < 0 || coll > matrix[0].length - 1) { return false; }
            if (matrix[row][coll] == target) {
                return true;
            } else if (matrix[row][coll] > target) {
                row--;
            } else if (matrix[row][coll] < target) {
                coll++;
            }
        }

        return false;
    }
}
