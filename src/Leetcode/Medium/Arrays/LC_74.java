package Leetcode.Medium.Arrays;

public class LC_74 {
    public static boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length == 0 || matrix[0].length == 0) { return false; }

        int rows = matrix.length;
        int colls = matrix[0].length;

        int left = 0;
        int right = rows * colls - 1;

        while (left <= right) {
            int middle = left + (right - left) / 2;
            int currElement = matrix[middle / colls][middle % colls];

            if (currElement == target) {
                return true;
            } else if (currElement < target) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }

        }

        return false;
    }
}
