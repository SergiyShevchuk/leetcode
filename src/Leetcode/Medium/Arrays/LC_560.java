package Leetcode.Medium.Arrays;

public class LC_560 {
    public int subarraySum(int[] nums, int k) {
        int counter = 0;
        for (int i = 0; i < nums.length; i++) {
            int summ = 0;
            for (int j = i; j < nums.length; j++) {
                summ += nums[j];
                if (summ == k) {
                    counter++;
                }
            }
        }

        return counter;
    }
}
