package Leetcode.Medium.Arrays;
import java.util.*;

public class LC_957 {
    public static int[] prisonAfterNDays(int[] cells, int N) {
        Map<Integer, int[]> states = new HashMap<>();
        int[] retValue = new int[cells.length];

        if (N > 255) {
            int tmp = N % 255;
            calculateStates(cells, 255, states);
            retValue = states.get(tmp - 1);
        } else {
            calculateStates(cells, N, states);
            retValue = states.get(N - 1);
        }

        return retValue;
    }

    private static void calculateStates(int[] cells,
                                        int days,
                                        Map<Integer, int[]> states) {
        for (int i = 0; i <= days; i++) {
            int[] newCellsState = nextDay(cells);
            states.put(i, newCellsState);
            cells = newCellsState;
        }
    }

    private static int[] nextDay(int[] cells) {
        int[] tmp = new int[cells.length];

        for (int i = 1; i < 7; i++) {
           tmp[i] = cells[i - 1] == cells[i + 1] ? 1 : 0;
        }

        return tmp;
    }
}
