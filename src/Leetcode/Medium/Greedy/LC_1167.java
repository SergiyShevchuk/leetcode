package Leetcode.Medium.Greedy;

import java.util.Arrays;
import java.util.PriorityQueue;

public class LC_1167 {
    public static int connectSticks(int[] sticks) {
        int result = 0;
        if (sticks.length == 0) {
            return result;
        }

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        for (Integer value: sticks) {
            priorityQueue.add(value);
        }

        while (priorityQueue.size() > 1) {
            int cost = priorityQueue.poll() + priorityQueue.poll();
            result += cost;
            priorityQueue.add(cost);
        }

        return result;
    }
}
