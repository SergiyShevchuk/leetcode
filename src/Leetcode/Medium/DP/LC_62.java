package Leetcode.Medium.DP;

import java.util.Arrays;

public class LC_62 {
    public static int uniquePaths(int m, int n) {
        int[][] area = new int[m][n];
        for (int row = 0; row < area.length; row++) {
            for (int coll = 0; coll < area[row].length; coll++) {
                area[row][coll] = 1;
            }
        }


        for (int coll = 1; coll < m; ++coll) {
            for (int row = 1; row < n; ++row) {
                area[coll][row] = area[coll - 1][row] + area[coll][row -1];
            }
        }

        return area[m - 1][n - 1];
    }
}
