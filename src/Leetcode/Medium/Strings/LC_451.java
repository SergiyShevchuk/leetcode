package Leetcode.Medium.Strings;

/* Sort Characters By Frequency

    Given a string, sort it in decreasing order based on the frequency of characters.
    Example 1:
    Input: "tree"
    Output: "eert"
 */

import java.util.*;

public class LC_451 {
    public static String frequencySort(String s) {
        if (s == null || s.length() <= 1) {
            return s;
        }

        HashMap<Character, Integer> storage = new HashMap<>();

        for (int index = 0; index < s.length(); index++) {
            Character curr = s.charAt(index);
            int amount = storage.getOrDefault(curr, 0) + 1;
            storage.put(curr, amount);
        }

        ArrayList<Integer> frequency = new ArrayList<>();

        for (Character key: storage.keySet()) {
            Integer value = storage.get(key);
            frequency.add(value);
        }

        Comparator comparator = Collections.reverseOrder();
        Collections.sort(frequency, comparator);

        StringBuilder builder = new StringBuilder();

        for (int index = 0; index < frequency.size(); index++) {
            Character maxChar = maxFrequency(storage, frequency.get(index));
            int frequencyIndex = storage.get(maxChar);
            storage.remove(maxChar, frequencyIndex);
            while (frequencyIndex != 0) {
                builder.append(maxChar);
                frequencyIndex--;
            }
        }

        return builder.toString();
    }


    public static Character maxFrequency(HashMap<Character, Integer> storage, int maxFrequency) {
        for (Character curr: storage.keySet()) {
            if (storage.get(curr) == maxFrequency) {
                return curr;
            }
        }

        return ' ';
    }
}
