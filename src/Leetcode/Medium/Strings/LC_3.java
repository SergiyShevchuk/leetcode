package Leetcode.Medium.Strings;
import java.util.*;

public class LC_3 {

    public static int lengthOfLongestSubstring(String s) {
        int maxValue = 0;

        if (s == null || s.length() == 0) { return maxValue; }
        if (s.length() == 1) { return 1; }

        for (int i = 0; i < s.length(); i++) {
            char[] storage = new char[256];
            storage[s.charAt(i)]++;
            for (int j = i + 1; j < Math.min(s.length(), i + 256 + 1); j++) {
                if (isSubstringQuniue(storage, s.charAt(j)) == false) {
                    maxValue = Math.max(maxValue, j - i);
                    break;
                }
            }
        }

        return maxValue;
    }

    private static boolean isSubstringQuniue(char[] storage, char current) {
        if (storage[current] != 0) { return false; }
        storage[current]++;
        return true;
    }



    // HashMap as data storage
    public int lengthOfLongestSubstringHashMap(String s) {
        int maxValue = 0;

        if (s == null || s.length() == 0) { return maxValue; }
        if (s.length() == 1) { return 1; }

        Set<Character> storage = new HashSet<>();
        int length = s.length();

        for (int i = 0; i < length; i++) {
            storage.clear();
            storage.add(s.charAt(i));
            for (int j = i + 1; j < Math.min(length, i + 1 + 256); j++) {
                if (storage.contains(s.charAt(j))) {
                    maxValue = Math.max(maxValue, storage.size());
                    break;
                } else {
                    storage.add(s.charAt(j));
                }
            }
            maxValue = Math.max(maxValue, storage.size());
        }

        return maxValue;
    }
}
