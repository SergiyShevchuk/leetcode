package Leetcode.Medium.Strings;

import java.util.*;

public class LC_692 {
    public static List<String> topKFrequent(String[] words, int k) {
        List<String> retValue = new LinkedList<>();
        if (k == 0) { return retValue; }

        Map<String, Integer> storage = new HashMap<>();

        for (String word: words) {
            storage.put(word, storage.getOrDefault(word, 0) + 1);
        }

        PriorityQueue<String> wordsHeap = new PriorityQueue<String>(
                (w1, w2) -> storage.get(w1).equals(storage.get(w2)) ?
                        w2.compareTo(w1) : storage.get(w1) - storage.get(w2));

        for (String word: storage.keySet()) {
            wordsHeap.offer(word);
            if (wordsHeap.size() > k) {
                wordsHeap.poll();
            }
        }

        return retValue;
    }
}
