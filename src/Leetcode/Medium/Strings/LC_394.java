package Leetcode.Medium.Strings;

/* 394. Decode String */

import java.util.Stack;

public class LC_394 {
    public static String decodeString(String s) {
        if (s == null || s.length() <= 1) { return s; }
        StringBuilder retValue = new StringBuilder();
        Stack<String> strStack = new Stack<>();
        Stack<Integer> counterStack = new Stack<>();

        int left = 0;

        while (left < s.length()) {
            Character current = s.charAt(left);
            if (Character.isDigit(current)) {
                int count = 0;
                while (Character.isDigit(s.charAt(left))) {
                    count = 10 * count + (s.charAt(left) - '0');
                    left++;
                }
                counterStack.add(count);
            } else if (current == '['){
                strStack.add(retValue.toString());
                retValue.setLength(0);
                left++;
            } else if (current == ']') {
                StringBuilder tmpBuilder = new StringBuilder(strStack.pop());
                Integer count = counterStack.pop();
                String aa = retValue.toString();
                for (int index = 0; index < count; index++) {
                    tmpBuilder.append(aa);
                }
                retValue.setLength(0);
                retValue.append(tmpBuilder.toString());
                left++;
            } else if (Character.isLetter(current)) {
                retValue.append(current);
                left++;
            }
        }

        return retValue.toString();
    }
}

/*

25 / 29 test cases passed.

public static String decodeString(String s) {
        if (s == null || s.length() <= 1) { return s; }

        StringBuilder strBuilder = new StringBuilder();
        for (int index = 0; index < s.length(); index++) {
             char current = s.charAt(index);
             if (Character.isDigit(current)) {
                 int rightEdge = index;
                 while (s.charAt(rightEdge) != ']') {
                     rightEdge++;
                 }
                 String tmp = singleBracketWord(s, "", index, rightEdge - 1);
                 index = rightEdge;
                 strBuilder.append(tmp);
             } else if (Character.isLetter(current)) {
                 strBuilder.append(current);
             }
        }

        return strBuilder.toString();
    }

    private static String singleBracketWord(String s, String rightPart, int left, int rightPointer) {
        char current = s.charAt(rightPointer);
        StringBuilder builder = new StringBuilder();
        while (s.charAt(rightPointer) != '[') {
            builder.append(s.charAt(rightPointer));
            rightPointer--;
        }
        rightPointer--;
        String tmp = builder.reverse().toString() + rightPart;
        builder.setLength(0);
        while (Character.isDigit(s.charAt(rightPointer))) {
            builder.append(s.charAt(rightPointer));
            if (rightPointer == 0) { break; }
            rightPointer--;
        }
        int count = Integer.valueOf(builder.reverse().toString());
        builder.setLength(0);
        for (int repeat = 0; repeat < count; repeat++) {
            builder.append(tmp);
        }

        String retV = builder.toString();

        if (left < rightPointer) {
            return singleBracketWord(s, retV, left, rightPointer);
        }

        return retV;
    }
 */