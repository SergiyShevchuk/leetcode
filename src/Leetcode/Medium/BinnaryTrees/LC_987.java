package Leetcode.Medium.BinnaryTrees;
import Common.DataStructures.TreeNode;
import java.util.*;

public class LC_987 {
    public List<List<Integer>> verticalTraversal(TreeNode root) {
        List<List<Integer>> finalList = new ArrayList<>();
        if (root == null) { return finalList; }
        if (root.left == null && root.right == null) {
            finalList.add(Arrays.asList(root.val));
            return finalList;
        }
        Map<List<Integer>, List<Integer>> coordinates = new HashMap<>();
        dfsTraverse(root, 0, 0, coordinates);

        for (List<Integer> position: coordinates.keySet()) {
            finalList.add(coordinates.get(position));
        }

        return finalList;
    }

    private void dfsTraverse(TreeNode root,
                             int x,
                             int y,
                             Map<List<Integer>, List<Integer>> coordinates) {
        if (root == null) { return; }
        dfsTraverse(root.left, x - 1, y - 1, coordinates);
        List<Integer> coord = Arrays.asList(x, y);
        if (!coordinates.containsKey(coord)) {
            List<Integer> list = new ArrayList<>();
            list.add(root.val);
            coordinates.put(coord, list);
        } else {
            List<Integer> list = coordinates.get(coord);
            list.add(root.val);
            coordinates.put(coord, list);
        }
        dfsTraverse(root.right, x + 1, y + 1, coordinates);
    }
}
