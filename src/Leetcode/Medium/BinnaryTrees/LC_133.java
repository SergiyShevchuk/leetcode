package Leetcode.Medium.BinnaryTrees;

import Common.DataStructures.GraphNode;
import java.util.*;

public class LC_133 {
    public GraphNode cloneGraph(GraphNode node) {
        if (node == null) { return null; }

        Map<GraphNode, GraphNode> clonesList = new HashMap<>();
        clonesList.put(node, new GraphNode(node.val));
        Queue<GraphNode> queue = new LinkedList<>();
        queue.add(node);

        while (!queue.isEmpty()) {
            GraphNode currNode = queue.poll();
            for (GraphNode neiborNode: currNode.neighbors) {
                if (!clonesList.containsKey(neiborNode)) {
                    clonesList.put(neiborNode, new GraphNode(neiborNode.val));
                    queue.add(neiborNode);
                }
                GraphNode neiborClone = clonesList.get(neiborNode);
                clonesList.get(currNode).neighbors.add(neiborClone);
            }
        }

        return clonesList.get(node);
    }
}
