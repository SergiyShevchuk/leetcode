package Leetcode.Medium.BinnaryTrees;

import Common.DataStructures.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class LC_1305 {
    public List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
        List<Integer> storage = new LinkedList<>();
        if (root1 == null && root2 == null) { return storage; }
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        traverse(root1, queue);
        traverse(root2, queue);

        while (!queue.isEmpty()) {
            storage.add(queue.poll());
        }

        return storage;
    }

    private void traverse(TreeNode root, PriorityQueue<Integer> queue) {
        if (root == null) { return; }
        traverse(root.left, queue);
        queue.add(root.val);
        traverse(root.right, queue);
    }
}
