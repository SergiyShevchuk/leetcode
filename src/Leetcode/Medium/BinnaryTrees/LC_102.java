package Leetcode.Medium.BinnaryTrees;

import Common.DataStructures.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/* 102. Binary Tree Level Order Traversal */

public class LC_102 {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> storage = new LinkedList<>();

        if (root == null) { return storage; }
        if (root.left == null && root.right == null) {
            List<Integer> zeroLevel = new LinkedList<>();
            zeroLevel.add(root.val);
            storage.add(zeroLevel);
            return storage;
        }

        Queue<List<TreeNode>> queue = new LinkedList<>();
        List<TreeNode> layer = new LinkedList<>();
        layer.add(root);
        queue.add(layer);

        while (!queue.isEmpty()) {
            List<TreeNode> currentLayer = queue.poll();
            List<TreeNode> nextLayer = new LinkedList<>();
            List<Integer> integers = new LinkedList<>();
            for (TreeNode node: currentLayer) {
                integers.add(node.val);
                if (node.left != null) { nextLayer.add(node.left); }
                if (node.right != null) { nextLayer.add(node.right); }
            }
            storage.add(integers);
            if (nextLayer.size() != 0) {
                queue.add(nextLayer);
            }

        }

        return storage;
    }
}
