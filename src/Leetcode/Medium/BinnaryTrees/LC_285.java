package Leetcode.Medium.BinnaryTrees;

import Common.DataStructures.ListNode;
import Common.DataStructures.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LC_285 {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        List<TreeNode> list = new ArrayList<>();
        constructTreeList(root, list);

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).val == p.val && i < list.size() - 2) {
                return list.get(i + 1);
            }
        }

        return null;
    }

    private void constructTreeList(TreeNode root, List<TreeNode> list) {
        if (root == null) {
            return;
        }
        constructTreeList(root.left, list);
        list.add(root);
        constructTreeList(root.right, list);
    }

    public TreeNode successor(TreeNode root, TreeNode p, TreeNode parent) {

        if (root.val < p.val) {
           return successor(root.right, p, root);
        } else if (root.val > p.val) {
           return successor(root.left, p, root);
        } else if (root.val == p.val) {

        }

        return null;
    }
}
