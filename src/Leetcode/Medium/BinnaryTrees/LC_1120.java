package Leetcode.Medium.BinnaryTrees;
import Common.DataStructures.TreeNode;
import java.util.*;

public class LC_1120 {
    public double maximumAverageSubtree(TreeNode root) {
        double result = 0.0;
        if (root == null) { return result; }
        if (root.left == null && root.right == null) {
            return (double)root.val;
        }

        Stack<TreeNode> stack = new Stack<>();
        stack.add(root);

        while (!stack.isEmpty()) {
            TreeNode current = stack.pop();
            int[] accu = new int[2];
            calculateSubtree(current, accu);
            result = Math.max(result, (double)accu[0]/(double)accu[1]);
            if (current.left != null) { stack.add(current.left); }
            if (current.right != null) { stack.add(current.right); }
        }

        return result;
    }


    private void calculateSubtree(TreeNode node, int[] accu) {
        if (node == null) { return; }
        accu[0] += node.val;
        accu[1] += 1;
        calculateSubtree(node.left, accu);
        calculateSubtree(node.right, accu);
    }
}

