package Leetcode.Medium.BinnaryTrees;

import Common.DataStructures.TreeNode;

public class LC_450 {
    public TreeNode deleteNode(TreeNode root, int key) {
        if (root == null) { return null; }
        if (root.val > key) {
            root.right = deleteNode(root.right, key);
        } else if (root.val < key) {
            root.left = deleteNode(root.left, key);
        } else {
            if (root.left == null) {
                return root.right;
            } else if (root.right == null) {
                return root.left;
            }
            TreeNode candidate = inorderTraverse(root.right);
            root.val = candidate.val;
            root.right = deleteNode(root.right, candidate.val);
        }

        return root;
    }

    private TreeNode inorderTraverse(TreeNode root) {
        if (root == null) {
            return null;
        }

        TreeNode pointer = root;
        while (pointer.left != null) {
            pointer = pointer.left;
        }

        return pointer;
    }
}
