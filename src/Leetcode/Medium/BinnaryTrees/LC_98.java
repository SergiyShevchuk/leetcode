package Leetcode.Medium.BinnaryTrees;

/* . Validate Binary Search Tree */

import Common.DataStructures.TreeNode;

public class LC_98 {
    public boolean isValidBST(TreeNode root) {
        return validation(root, null, null);
    }


    // 1 2 3
    private boolean validation(TreeNode root, Integer left, Integer right) {
        if (root == null) { return true; }
        int val = root.val;

        if (left != null && val <= left) { return false; }
        if (right != null && val >= right) { return false; }

        if (validation(root.left, left, val) == false) { return false; }
        if (validation(root.right, val, right) == false) { return false; }

        return true;
    }

    public boolean helper(TreeNode node, Integer lower, Integer upper) {
        if (node == null) return true;

        int val = node.val;
        if (lower != null && val <= lower) return false;
        if (upper != null && val >= upper) return false;

        if (! helper(node.right, val, upper)) return false;
        if (! helper(node.left, lower, val)) return false;
        return true;
    }
}
