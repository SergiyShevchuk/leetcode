package Leetcode.Medium.Backtracking;
import java.util.*;

public class LC_17 {

    public static List<String> letterCombinations(String digits) {
        List<String> list = new ArrayList<>();

        if (digits == null || digits.length() == 0) { return list; }
        Map<Integer, String> digitsBook = digitToPossibleLetters();
        combinations(digits, 0, new StringBuilder(), list, digitsBook);

        return list;
    }

    private static void combinations(String digits,
                              int currentIndex,
                              StringBuilder builder,
                              List<String> list,
                              Map<Integer, String> digitsBook) {
        if (currentIndex == digits.length()) {
            list.add(builder.toString());
            return;
        }

        char currLetter = digits.charAt(currentIndex);
        int currDigit = currLetter - '0';
        String digitLetters = digitsBook.get(currDigit);

        for (char digitValue: digitLetters.toCharArray()) {
            builder.append(digitValue);
            combinations(digits, currentIndex + 1, builder, list, digitsBook);
            builder.deleteCharAt(builder.length() - 1);
        }
    }

    private static Map<Integer, String> digitToPossibleLetters() {
      Map<Integer, String> retValue = new HashMap<>();

        retValue.put(0, "");
        retValue.put(1, "");
        retValue.put(2, "abc");
        retValue.put(3, "def");
        retValue.put(4, "ghi");
        retValue.put(5, "jkl");
        retValue.put(6, "mno");
        retValue.put(7, "pqrs");
        retValue.put(8, "tuv");
        retValue.put(9, "wxyz");

        return retValue;
    }
}
