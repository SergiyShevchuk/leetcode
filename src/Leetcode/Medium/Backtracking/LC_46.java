package Leetcode.Medium.Backtracking;
import java.util.*;

public class LC_46 {
    public static List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> storage = new ArrayList<>();
        boolean[] visited = new boolean[nums.length];
        Arrays.sort(nums);
        combinations(nums, new ArrayList<>(), storage, visited);

        return storage;
    }

    private static void combinations(int[] nums,
                              List<Integer> currState,
                              List<List<Integer>> storage,
                                     boolean[] visited) {
        if (currState.size() == nums.length) {
            storage.add(new ArrayList<>(currState));
            return;
        }

        for (int i = 0; i < nums.length; i++) {

            if (visited[i]) { continue; }
            // number is the same as the previous one, avoid reusing it
            if (i > 0 && nums[i] == nums[i - 1] && !visited[i - 1]) {
                continue;
            }

            int currentChoise = nums[i];
            visited[i] = true;
            currState.add(currentChoise);
            combinations(nums, currState, storage, visited);
            currState.remove(currState.size() - 1);
            visited[i] = false;
        }
    }
}
