package Leetcode.Medium.Backtracking;
import java.util.*;

public class LC_47 {
    public static List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> storage = new ArrayList<>();

        combinations(nums, storage, new ArrayList<>());

        return storage;
    }

    private static void combinations(int[] nums, List<List<Integer>> storage, List<Integer> currState) {
        if (nums.length == currState.size()) {
            storage.add(new ArrayList<>(currState));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            int currDigit = nums[i];
            if (currState.contains(currDigit)) { continue; }
            currState.add(currDigit);
            combinations(nums, storage, currState);
            currState.remove(currState.size() - 1);
        }
    }
}
