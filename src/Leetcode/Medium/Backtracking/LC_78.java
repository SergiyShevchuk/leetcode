package Leetcode.Medium.Backtracking;
import java.util.*;

public class LC_78 {
    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> storage = new ArrayList<>();

        conbinatios(0, nums, storage, new ArrayList<>());

        return storage;
    }

    private static void conbinatios(int index,
                             int[] nums,
                             List<List<Integer>> storage,
                             List<Integer> currentCombi) {
        if (index == nums.length) {
            storage.add(new ArrayList<>(currentCombi));
            return;
        }

        currentCombi.add(nums[index]);
        conbinatios(index + 1, nums, storage, currentCombi);
        currentCombi.remove(currentCombi.size() - 1);
        conbinatios(index + 1, nums, storage, currentCombi);
    }
}
