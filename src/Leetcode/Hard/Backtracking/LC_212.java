package Leetcode.Hard.Backtracking;

import java.util.*;

public class LC_212 {
    public static List<String> findWords(char[][] board, String[] words) {
        List<String> wordsList = new ArrayList<>();
        if (words == null || words.length == 0) {
            return wordsList;
        }

        HashMap<String, Set<int[]>> wordsCoordinates = new HashMap<>();


        for (String word: words) {
            checkIsWordExist(board, word, wordsCoordinates);
        }

        Set<String> quniueWords = new HashSet<>();
        Set<String> existedWords = wordsCoordinates.keySet();

        for (String word: existedWords) {
                Set<int[]> parentCoordinates = wordsCoordinates.get(word);
                for (String comparable: existedWords) {
                    if (!word.equals(comparable)) {
                        Set<int[]> comparableCoordinates = wordsCoordinates.get(comparable);
                        if (parentCoordinates.retainAll(comparableCoordinates)) {
                            quniueWords.add(word);
                            quniueWords.add(comparable);
                        }
                    }
                }
        }


        wordsList.addAll(quniueWords);
        return wordsList;
    }

    private static void checkIsWordExist(char[][] board, String word, HashMap<String, Set<int[]>> wordsCoordinates) {
        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == word.charAt(0)) {
                    Stack<int[]> coordinates = new Stack<>();
                    if (isWordExist(board, word, 0, i, j, coordinates, directions) == true) {
                        Set<int[]> storage = new HashSet<>();
                        constructWordBack(board, word, coordinates, storage);
                        wordsCoordinates.put(word, storage);
                        return;
                    }
                }
            }
        }
    }

    private static void constructWordBack(char[][] board, String word, Stack<int[]> coordinates, Set<int[]> storage) {
        for (int i = word.length() - 1; i >= 0; i--) {
            int[] coordinate = coordinates.pop();
            storage.add(coordinate);
            board[coordinate[0]][coordinate[1]] = word.charAt(i);
        }
    }

    private static boolean isWordExist(char[][] board, String word, int counter, int i, int j, Stack<int[]> coordinates, int[][] directions) {

        if (i < 0 || i >= board.length || j < 0 || j >= board[i].length || board[i][j] != word.charAt(counter)) {
            return false;
        }

        if (counter == word.length() - 1) {
            coordinates.add(new int[] {i, j});
            return true;
        }

        coordinates.add(new int[] {i, j});
        board[i][j] = '#';

        for (int[] direction: directions) {
            if (isWordExist(board, word, counter + 1, i + direction[0], j + direction[1], coordinates, directions) == true) {
                return true;
            }
        }

        coordinates.pop();
        board[i][j] = word.charAt(counter);

        return false;
    }
}
