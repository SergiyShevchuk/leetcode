package Leetcode.Hard;

import Common.DataStructures.ListNode;

import java.util.HashMap;
import java.util.TreeSet;

public class LC_23 {
    public static ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }

        if (lists.length == 1) {
            return lists[0];
        }


        TreeSet<Integer> nodeIndexs = new TreeSet<>();
        HashMap<Integer, Integer> storage = new HashMap<>();

        for (ListNode node: lists) {
            while (node != null) {
                nodeIndexs.add(node.val);
                Integer indexs = storage.getOrDefault(node.val, 0);
                storage.put(node.val, indexs + 1);
                node = node.next;
            }
        }

        ListNode head = null;
        ListNode firstHead = null;

        for (Integer nodeIndex: nodeIndexs) {
            Integer count = storage.get(nodeIndex);
            for (int index = 0; index < count; index++) {
                if (head == null) {
                    head = new ListNode(nodeIndex);
                    firstHead = head;
                } else {
                    head.next = new ListNode(nodeIndex);
                    head = head.next;
                }
            }
        }

        return firstHead;
    }

    public ListNode mergeKListsIteration(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }

        if (lists.length == 1) {
            return lists[0];
        }

        ListNode first = merge(lists[0], lists[1]);

        for (int index = 2; index < lists.length; index++) {
           first = merge(first, lists[index]);
        }

        return first;
    }

    private ListNode merge(ListNode l1, ListNode l2) {

        ListNode prehead = new ListNode(-1);
        ListNode prev = prehead;

        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val) {
                prev.next = l1;
                l1 = l1.next;
            } else {
                prev.next = l2;
                l2 = l2.next;
            }
            prev = prev.next;
        }

        prev.next = l1 == null ? l2 : l1;

        return prehead.next;
    }
}
