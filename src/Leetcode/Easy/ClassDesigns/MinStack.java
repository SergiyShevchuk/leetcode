package Leetcode.Easy.ClassDesigns;

import java.util.Stack;

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */

public class MinStack {

    private Stack<Integer> storage;
    int minElement = Integer.MAX_VALUE;

    public MinStack() {
        this.storage = new Stack<>();
    }

    public void push(int x) {
        storage.add(x);
        minElement = Math.min(minElement, x);
    }

    public void pop() {
        Integer current = storage.pop();
        if (current == minElement) {
            minElement = Integer.MAX_VALUE;
            for (Integer value: storage.subList(0, storage.size())) {
                minElement = Math.min(minElement, value);
            }
        }
    }

    public int top() {
       return storage.peek();
    }

    public int getMin() {
        return minElement;
    }
}
