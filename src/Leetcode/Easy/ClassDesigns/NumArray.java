package Leetcode.Easy.ClassDesigns;

// LC_303

public class NumArray {

    private int[] numsStorage;

    public NumArray(int[] nums) {
       numsStorage = nums;
    }

    public int sumRange(int i, int j) {
        if (numsStorage == null || numsStorage.length == 0) {
            throw new IllegalArgumentException("Inputed array of nums is empty");
        }

        int lenght = numsStorage.length;

        if ( i > lenght || j > lenght) {
            throw new IllegalArgumentException("Inputed indeces in invalid");
        }

        if (lenght == 1) {
            return numsStorage[0];
        }

        if (i == j) {
            return numsStorage[i];
        }

        int summ = 0;

        for (int index = i; index <= j; index++) {
         summ += numsStorage[index];
        }

        return summ;
    }
}
