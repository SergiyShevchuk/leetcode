package Leetcode.Easy.LinkedList;

/* Palindrome Linked List
*  Given a singly linked list, determine if it is a palindrome.
* */

import Common.DataStructures.ListNode;

import java.util.ArrayList;

public class LC_234 {
    public boolean isPalindrome(ListNode head) {

        if (head == null || head.next == null) {
            return true;
        }

        ArrayList<ListNode> storage = new ArrayList<>();
        ListNode slow = head;

        while (slow != null) {
            storage.add(slow);
            slow = slow.next;
        }

        int size = storage.size();
        for (int index = 0; index < size / 2; index++) {
            ListNode first = storage.get(index);
            ListNode last = storage.get(size - index - 1);
            if (last.val != first.val) {
                return false;
            }
        }

        return true;
    }

    public static boolean isPalindromeConstantSpace(ListNode head) {

        if (head == null || head.next == null) {
            return true;
        }

        if (head.next.next == null) {
            return head.val == head.next.val;
        }

        ListNode middlePoint = halfList(head);
        ListNode reversePoit = reversedHalfList(head);

        while (middlePoint != null && reversePoit != null) {

            if (middlePoint.val != reversePoit.val) {
                return false;
            }

            middlePoint = middlePoint.next;
            reversePoit = reversePoit.next;
        }

        return true;
    }

    private static ListNode halfList(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }

        return slow;
    }

    private static ListNode reversedHalfList(ListNode head) {
        ListNode curr = head;
        ListNode prev = null;

        while (curr != null) {
            ListNode tmp = curr.next;
            curr.next = prev;
            prev = curr;
            curr = tmp;
        }

        return prev;
    }
}
