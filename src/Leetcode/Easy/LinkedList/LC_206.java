package Leetcode.Easy.LinkedList;

import Common.DataStructures.ListNode;

import java.util.Stack;

public class LC_206 {
    public ListNode reverseList(ListNode head) {

        return  head;
    }

    private ListNode recirsionReverse(ListNode head) {
        if (head.next == null || head == null) {
            return head;
        }
        ListNode prev = recirsionReverse(head.next);
        head.next.next = head;
        head.next = null;

        return  prev;
    }

    private ListNode iterativeReverse(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode prev = null;
        ListNode curr = head;

        while (curr != null) {
            ListNode next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }

        return prev;
    }

    private ListNode stackReverse(ListNode head) {
        if (head.next == null) {
            return head;
        }

        Stack<ListNode> storage = new Stack<>();

        ListNode prev = recirsionReverse(head.next);
        head.next.next = head;
        head.next = null;

        return  prev;
    }
}
