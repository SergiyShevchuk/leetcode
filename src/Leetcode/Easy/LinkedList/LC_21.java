package Leetcode.Easy.LinkedList;
import Common.DataStructures.ListNode;

public class LC_21 {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) { return null; }
        if (l1 == null) { return l2; }
        if (l2 == null) { return l1; }

        ListNode revers1 = reverseList(l1);
        ListNode revers2 = reverseList(l2);

        ListNode newHead = null;
        int diff = revers1.val + revers2.val;
        if (diff >= 10) {
            newHead = new ListNode(diff - 10);
            diff = diff - 10;
        } else {
            newHead = new ListNode(diff);
            diff = 0;
        }


        return null;
    }

    private ListNode reverseList(ListNode node) {
        if (node == null || node.next == null) {
            return node;
        }

        ListNode curr = node;
        ListNode last = null;

        while (curr != null) {
            ListNode tmp = curr.next;
            curr.next = last;
            last = curr;
            curr = tmp;
        }

        return last;
    }
}
