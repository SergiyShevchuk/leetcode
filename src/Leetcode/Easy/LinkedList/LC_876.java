package Leetcode.Easy.LinkedList;

import Common.DataStructures.ListNode;

public class LC_876 {
    public ListNode middleNode(ListNode head) {
        if (head == null || head.next.next == null) {
            return head;
        }

        ListNode slow = head;
        ListNode fast = head.next.next;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        return slow;
    }
}
