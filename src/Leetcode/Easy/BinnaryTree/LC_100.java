package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

import java.util.ArrayList;

public class LC_100 {
    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) { return true; }
        boolean flag = checkIsSame(p, q);
        return flag;
    }

    private static boolean checkIsSame(TreeNode p, TreeNode q) {

        if (p == null && q == null) { return true; }
        if (p == null || q == null) { return false; }
        if (p.val != q.val) { return false; }
        return checkIsSame(p.left, q.left) && checkIsSame(p.right, q.right);
    }

    private static void traverseNodesList(TreeNode p, ArrayList<Integer> list) {
        if (p == null) { return; }

        traverseNodesList(p.left, list);
        list.add(p.val);
        traverseNodesList(p.right, list);
    }
}
