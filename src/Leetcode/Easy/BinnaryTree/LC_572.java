package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

import java.util.Stack;

public class LC_572 {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        Stack<TreeNode> storage = new Stack<>();
        storage.add(s);

        while (!storage.isEmpty()) {
            TreeNode curr = storage.pop();

            if (isEaqual(curr, t)) {
                return true;
            }

            if (curr.left != null) {
                storage.add(curr.left);
            }

            if (curr.right != null) {
                storage.add(curr.right);
            }
        }

        return false;
    }

    public boolean isEaqual(TreeNode s, TreeNode t) {
        if (s == null && t == null) {
            return true;
        }

        if (s == null || t == null) {
            return false;
        }

        if (s.val != t.val) {
            return false;
        }

        return isEaqual(s.left, t.left) && isEaqual(s.right, t.right);
    }
}
