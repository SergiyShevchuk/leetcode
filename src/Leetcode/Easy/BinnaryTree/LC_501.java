package Leetcode.Easy.BinnaryTree;

/*
Find Mode in Binary Search Tree
 */

import Common.DataStructures.TreeNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class LC_501 {
    public int[] findMode(TreeNode root) {
        Set<Integer> list = new HashSet<>();
        traverse(root, list);
        int[] a = new int[list.size()];
        int index = 0;
        for (Integer value: list) {
            a[index] = value;
            index++;
        }
        return a;
    }

    private void traverse(TreeNode root, Set<Integer> list) {
        if (root == null) { return; }

    }

}
