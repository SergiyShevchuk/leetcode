package Leetcode.Easy.BinnaryTree;

/*
Leaf-Similar Trees
 */

import Common.DataStructures.TreeNode;

import java.util.ArrayList;

public class LC_872 {

    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        ArrayList<TreeNode> listLeft = new ArrayList<>();
        ArrayList<TreeNode> rightLeft = new ArrayList<>();

        leafsList(root1, listLeft);
        leafsList(root2, rightLeft);

        if (listLeft.size() != rightLeft.size()) {
            return false;
        }

        for (int i = 0; i < listLeft.size(); i++) {
            if (listLeft.get(i).val != rightLeft.get(i).val) {
                return false;
            }
        }

        return true;
    }

    private void leafsList(TreeNode root, ArrayList<TreeNode> list) {
        if (root == null) { return; }
        if (root.left == null && root.right == null) {
            list.add(root);
        }
        leafsList(root.left, list);
        leafsList(root.right, list);
    }

//    private boolean isLeafSimilar(TreeNode root1, TreeNode root2) {
//
//        if ((root1.left == null && root1.right == null) && (root2.left == null && root2.right == null)) {
//
//        }
//
//        return false;
//    }
}
