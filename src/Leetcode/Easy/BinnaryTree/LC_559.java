package Leetcode.Easy.BinnaryTree;

/* 559. Maximum Depth of N-ary Tree */

import Common.DataStructures.BinnaryNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_559 {
    public int maxDepth(BinnaryNode root) {
        int deep = 0;
        if (root == null) {
            return deep;
        }

        if (root.children.size() == 0) {
            return 1;
        }

        Queue<ArrayList<BinnaryNode>> queue = new LinkedList<>();
        ArrayList<BinnaryNode> list = new ArrayList<>();
        list.add(root);
        queue.add(list);

        while (!queue.isEmpty()) {
            deep++;
            ArrayList<BinnaryNode> currentLayer = queue.poll();
            ArrayList<BinnaryNode> nextLayer = new ArrayList<>();
            for (BinnaryNode node: currentLayer) {
                if (node.children.size() != 0) {
                    nextLayer.addAll(node.children);
                }
            }
            if (nextLayer.size() != 0) {
                queue.add(nextLayer);
            }
        }

        return deep;
    }
}
