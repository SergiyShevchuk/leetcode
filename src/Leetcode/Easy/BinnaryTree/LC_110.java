package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

public class LC_110 {
    public boolean isBalanced(TreeNode root) {
        if (root == null) { return true; }

        return Math.abs(heightChecker(root.left) - heightChecker(root.right)) < 2
                && isBalanced(root.left)
                && isBalanced(root.right);
    }

    private int heightChecker(TreeNode root) {
        if (root == null) { return -1; }

        return Math.max(heightChecker(root.left), heightChecker(root.right)) + 1;
    }
}
