package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class LC_938 {
    public int rangeSumBST(TreeNode root, int L, int R) {
       if (root == null) { return 0; }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        int summ = 0;
        while (!queue.isEmpty()) {
            TreeNode current = queue.poll();
            if (L <= current.val && current.val <= R) {
                summ += current.val;
            }
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }

        return summ;
    }

}
