package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

public class LC_108 {


    // Inorder Traversal
    public static TreeNode sortedArrayToBST(int[] nums) {
        if (nums == null || nums.length == 0) { return null; }
        if (nums.length == 1) { TreeNode node = new TreeNode(nums[0]); }

        return constructTree(0, nums.length - 1, nums);
    }

    private static TreeNode constructTree(int left, int right, int[] nums) {
        if (left > right) { return null; }
        int midd = (left + right) / 2;
        TreeNode root = new TreeNode(nums[midd]);
        root.left = constructTree(left, midd - 1, nums);
        root.right = constructTree(midd + 1, right, nums);
        return root;
    }
}
