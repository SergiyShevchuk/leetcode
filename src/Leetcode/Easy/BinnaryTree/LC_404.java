package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

import java.util.Stack;

public class LC_404 {
    public int sumOfLeftLeaves(TreeNode root) {
        int sum = 0;
        if (root == null || (root.left == null && root.right == null)) {
            return sum;
        }

        Stack<TreeNode> storage = new Stack<>();
        storage.add(root);
        while (!storage.isEmpty()) {
            TreeNode curr = storage.pop();
            if (curr.left != null) {
                if (curr.left.left == null && curr.left.right == null) {
                    sum += curr.left.val;
                } else {
                    storage.add(curr.left);
                }
            }
            if (curr.right != null) {
                storage.add(curr.right);
            }
        }

        return sum;
    }
}
