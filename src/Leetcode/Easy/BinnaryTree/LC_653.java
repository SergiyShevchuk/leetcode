package Leetcode.Easy.BinnaryTree;
import Common.DataStructures.TreeNode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeSet;

public class LC_653 {
    public boolean findTarget(TreeNode root, int k) {

        Deque<TreeNode> deque = new LinkedList<>();
        deque.add(root);
        while (!deque.isEmpty()) {
            TreeNode node = deque.pollFirst();
            int nodeVal = node.val;
            int diff = k - node.val;
            if (diff > node.val) {
                if (isNodeExist(root.right, diff, node)) {
                    return true;
                }
            } else {
                if (isNodeExist(root.left, diff, node)) {
                    return true;
                };
            }
            if (node.left != null) { deque.add(node.left); }
            if (node.right != null) { deque.add(node.right); }
        }

        return false;
    }

    private boolean isNodeExist(TreeNode node, int k, TreeNode root) {
        if (node == null) { return false; }
        if (k == node.val && root != node) { return true; }

        if (k > node.val) {
            return isNodeExist(node.right, k, root);
        }

        return isNodeExist(node.left, k, root);
    }
}
