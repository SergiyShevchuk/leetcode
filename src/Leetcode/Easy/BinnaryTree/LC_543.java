package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

public class LC_543 {
    public static int diameterOfBinaryTree(TreeNode root) {
        if (root == null) {
            return -1;
        }
        int left = diameterOfBinaryTree(root.left);
        int right = diameterOfBinaryTree(root.right);
        return Math.max(left, right) + 1;
    }
}
