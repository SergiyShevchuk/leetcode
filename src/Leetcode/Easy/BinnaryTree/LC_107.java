package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class LC_107 {
    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> storage = new LinkedList<>();
        List<Integer> tmpLayer = new LinkedList<>();
        if (root == null) { return storage; }
        if (root.left == null && root.right == null) {
            tmpLayer.add(root.val);
            storage.add(tmpLayer);
        }

        Stack<List<Integer>> stack = new Stack<>();
        Queue<List<TreeNode>> queue = new LinkedList<>();
        List<TreeNode> layer = new LinkedList<>();
        layer.add(root);
        queue.add(layer);

        while (!queue.isEmpty()) {
            List<TreeNode> currLayer = queue.poll();
            List<TreeNode> nextLayer = new LinkedList<>();
            List<Integer> intLayer = new LinkedList<>();
            for (TreeNode node: currLayer) {
                intLayer.add(node.val);
                if (node.left != null) {
                    nextLayer.add(node.left);
                }
                if (node.right != null) {
                    nextLayer.add(node.right);
                }
            }
            stack.add(intLayer);
            if (nextLayer.size() != 0) {
                queue.add(nextLayer);
            }
        }

        while (!stack.isEmpty()) {
            storage.add(stack.pop());
        }

        return storage;
    }
}
