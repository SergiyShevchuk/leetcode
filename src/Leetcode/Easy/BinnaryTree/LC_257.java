package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

import java.util.LinkedList;
import java.util.List;

public class LC_257 {
    public List<String> binaryTreePaths(TreeNode root) {
        // Check if root == null

        List<String> storage = new LinkedList<>();

        if (root.left == null && root.right == null) {
           storage.add(String.valueOf(root.val));
           return storage;
        }

        StringBuffer buidlerLeft = new StringBuffer();
        StringBuffer buidlerRight = new StringBuffer();
        buidlerLeft.append(String.valueOf(root.val));
        buidlerRight.append(String.valueOf(root.val));

        createPaths(root.left, buidlerLeft, storage);
        createPaths(root.right, buidlerRight, storage);

        return storage;
    }

    private void createPaths(TreeNode root, StringBuffer buidler, List<String> storage) {
        if (root == null) { return; }
        buidler.append("->" + String.valueOf(root.val));

        if (root.left == null && root.right == null) {
            buidler.append("->" + String.valueOf(root.val));
            storage.add(buidler.toString());
            return;
        }
        
        createPaths(root.left, buidler, storage);
        createPaths(root.right, buidler, storage);
    }
}
