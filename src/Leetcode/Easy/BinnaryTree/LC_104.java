package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LC_104 {

    // Recursive traverse of tree and depth calculation
    public int maxDepthRecursive(TreeNode root) {
        if (root == null) { return 0; }

        return 0;
    }

    public int treeDepth(TreeNode root) {
        if (root == null) { return 0; }
        return Math.max(treeDepth(root.left) + 1, treeDepth(root.right) + 1);
    }

    // BFS traverse of tree and depth calculation
    public int maxDepth(TreeNode root) {
        int depth = 0;
        if (root == null) {
            return depth;
        }

        if (root.left == null && root.right == null) {
            return 1;
        }

        Queue<ArrayList<TreeNode>> queue = new LinkedList<>();
        ArrayList<TreeNode> nodesList = new ArrayList<>();
        nodesList.add(root);
        queue.add(nodesList);

        while(!queue.isEmpty()) {
            depth++;
            ArrayList<TreeNode> currentLayer = queue.poll();
            ArrayList<TreeNode> nextLayer = new ArrayList<>();
            for (TreeNode currNode: currentLayer) {
                if (currNode.left != null) {
                    nextLayer.add(currNode.left);
                }
                if (currNode.right != null) {
                    nextLayer.add(currNode.right);
                }
            }
            if (nextLayer.size() != 0) {
                queue.add(nextLayer);
            }
        }

        return depth;
    }

}
