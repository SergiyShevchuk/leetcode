package Leetcode.Easy.BinnaryTree;

import Common.DataStructures.TreeNode;

public class LC_563 {
    public int findTilt(TreeNode root) {
        int[] storage = new int[1];
        dfs(root, storage);

        return storage[0];
    }

    private int dfs(TreeNode root, int[] storage) {
        if (root == null) { return 0; }
        int left = dfs(root.left, storage);
        int right = dfs(root.right, storage);
        storage[0] = Math.abs(left - right);
        return left + right + root.val;
    }

}
