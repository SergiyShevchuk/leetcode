package Leetcode.Easy;

public class LC_1266 {
    public static int minTimeToVisitAllPoints(int[][] points) {

        int summ = 0;

        if (points == null) {
            return summ;
        }

        if (points.length == 0 || points.length == 1) {
            return summ;
        }

        for (int index = 0; index < points.length; index++) {
            if (index == points.length - 1) {
                break;
            }

            summ += calculations(points[index], points[index + 1]);
        }

        return summ;
    }

    private static int calculations(int[] left, int[] right) {
            int xDiff = Math.abs(right[0] - left[0]);
            int yDiff = Math.abs(right[1] - left[1]);
            int diff = Math.abs(xDiff - yDiff);

            return Math.min(xDiff, yDiff) + diff;
    }
}
