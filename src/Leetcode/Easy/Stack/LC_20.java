package Leetcode.Easy.Stack;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class LC_20 {
    public static boolean isValid(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }

        if (s.length() == 1) {
            return false;
        }

        Set<Character> left = new HashSet<>();
        left.add('(');
        left.add('{');
        left.add('[');

        Stack<Character> storage = new Stack<>();

        for (int index = 0; index < s.length(); index++) {
            Character current = s.charAt(index);

            if (storage.isEmpty() == true && left.contains(current) == false) {
                return false;
            }

            if (left.contains(current)) {
                storage.add(current);
            } else {
             Character stackCurrent = storage.pop();
             if (current == ')' && stackCurrent != '(') {
                 return false;
             } else if (current == '}' && stackCurrent != '{') {
                 return false;
             } else if (current == ']' && stackCurrent != '[') {
                 return false;
             }
            }
        }

        return storage.empty();
    }
}
