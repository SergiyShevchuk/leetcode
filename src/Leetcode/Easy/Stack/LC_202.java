package Leetcode.Easy.Stack;

import java.util.HashSet;
import java.util.*;

public class LC_202 {
    public static boolean isHappy(int n) {
        if (n == 1) { return true; }

        int retValue = computeNumber(n);
        Set<Integer> set = new HashSet<>();
        set.add(retValue);

        boolean isCycle = false;

        while (isCycle == false) {
            if (retValue == 1) {
                return true;
            }
            retValue = computeNumber(retValue);
            if (set.contains(retValue)) {
                isCycle = true;
            } else {
                set.add(retValue);
            }
        }

        return false;
    }

    private static int computeNumber(int n) {
        Stack<Integer> stack = new Stack<>();
        while (n != 0) {
            int tmp = n % 10;
            stack.add((int)Math.pow(tmp, 2));
            n = (n - tmp) / 10;
        }

        int nextValue = 0;

        while (!stack.isEmpty()) {
            nextValue += stack.pop();
        }

        return nextValue;
    }
}
