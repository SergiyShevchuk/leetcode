package Leetcode.Easy.Strings;

/* 125. Valid Palindrome */

public class LC_125 {
    public static boolean isPalindrome(String s) {
        if (s == null || s.length() == 0 || s.length() == 1) {
            return true;
        }

        int left = 0;
        int right = s.length() - 1;

        while (left < right) {

            if (Character.isLetterOrDigit(s.charAt(left))) {
                if (Character.isLetterOrDigit(s.charAt(right))) {

                    if (Character.toLowerCase(s.charAt(left)) != Character.toLowerCase( s.charAt(right))) {
                        return false;
                    }
                    right--;
                    left++;

                } else {
                   right--;
                }
            } else {
                left++;
            }
        }

        return true;
    }
}
