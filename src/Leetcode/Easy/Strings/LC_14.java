package Leetcode.Easy.Strings;

import java.util.HashMap;
import java.util.Map;

public class LC_14 {
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) { return ""; }

        Map<String, Integer> map = new HashMap<>();
        for (String word: strs) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < word.length(); i++) {
                builder.append(word.charAt(i));
                String prefix = builder.toString();
                if (map.containsKey(prefix)) {
                    map.put(prefix, map.get(prefix) + 1);
                } else {
                    map.put(prefix, 1);
                }
            }
        }

        String retValue = "";
        Integer maxPrefix = 1;
        for (String prefix: map.keySet()) {
            if (map.get(prefix) >= maxPrefix) {
                maxPrefix = map.get(prefix);
                retValue = prefix;
            }
        }

        return retValue;
    }
}
