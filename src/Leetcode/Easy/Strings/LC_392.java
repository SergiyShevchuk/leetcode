package Leetcode.Easy.Strings;

public class LC_392 {
    public static boolean isSubsequence(String s, String t) {
        if (s.length() > t.length()) {
            return false;
        }

        if (s == null && t == null) {
            return true;
        }

        if ((s == null && t != null) || (s != null && t == null)) {
            return false;
        }

        int extPointer = 0;
        int sLength = s.length();

        for (int inner = 0; inner < s.length(); inner++) {
            char currensS = s.charAt(inner);
            for (int ext = extPointer; ext < t.length(); ext++) {
                extPointer++;
                char tChar = t.charAt(ext);
                if (tChar == currensS) {
                    sLength--;
                    break;
                }
            }
        }

        Boolean aaa = sLength == 0;

        return aaa;
    }
}
