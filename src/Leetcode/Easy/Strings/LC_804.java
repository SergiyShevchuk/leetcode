package Leetcode.Easy.Strings;

// 804. Unique Morse Code Words

import java.util.HashSet;
import java.util.Set;

public class LC_804 {
    public static int uniqueMorseRepresentations(String[] words) {

        if (words == null || words.length == 0) {
            return 0;
        }

        String[] dotsAlphabet = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};

        Set<String> storage = new HashSet<>();
        StringBuilder builder = new StringBuilder();

        for (String word: words) {
            for (int index = 0; index < word.length(); index++) {
                char current = word.charAt(index);
                int charIndex = current - 97;
                String morzeLetter = dotsAlphabet[charIndex];
                builder.append(morzeLetter);
            }
            storage.add(builder.toString());
            builder.setLength(0);
        }

        return storage.size();
    }
}
