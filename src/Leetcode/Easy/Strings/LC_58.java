package Leetcode.Easy.Strings;

public class LC_58 {
    public static int lengthOfLastWord(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        if (s.length() == 1 && s.charAt(0) != ' ') {
            return 1;
        }

        if (s.length() == 1 && s.charAt(0) == ' ') {
            return 0;
        }

        String[] storage = s.split(" ");

        if (storage.length == 0) {
            return 0;
        }

        String last = storage[storage.length - 1];

        int test = last.length();

        return test;
    }
}
