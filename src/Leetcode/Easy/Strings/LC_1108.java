package Leetcode.Easy.Strings;

public class LC_1108 {
    public String defangIPaddr(String address) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < address.length(); i++) {
            char current = address.charAt(i);
            if (current != '.') {
                builder.append(current);
            } else {
                builder.append("[.]");
            }
        }

        return builder.toString();
    }
}
