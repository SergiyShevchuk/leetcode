package Leetcode.Easy.Strings;

/*   First Unique Character in a String

    Given a string, find the first non-repeating
    character in it and return it's index. If it doesn't exist, return -1.  */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class LC_387 {
    public static int firstNonRepeatedCharacter(String str) {

        if (str == null || str.length() == 0) {
            return -1;
        }

        char[] storage = new char[26];
        HashMap<Character, Integer> charIndexes = new HashMap<>();
        ArrayList<Character> aList = new ArrayList<>();
        Set<Character> charsList = new HashSet<>();

        for (int index = 0; index < str.length(); index++) {
            Character curr = str.charAt(index);
            storage[curr - 'a']++;
            if (!charsList.contains(curr)) {
                charIndexes.put(curr, index);
                charsList.add(curr);
                aList.add(curr);
            }
        }

        for (int index = 0; index < aList.size(); index++) {
            Character curr = aList.get(index);
            if (storage[curr - 'a'] == 1) {
                Integer charIndex = charIndexes.get(curr);
                return charIndex;
            }
        }

        return -1;
    }
}
