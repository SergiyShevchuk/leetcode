package Leetcode.Easy.Strings;

public class LC_299 {
    public static String getHint(String secret, String guess) {
        if (secret == null || guess == null) {
            return null;
        }

        if (secret.length() != guess.length()) {
            return null;
        }

        int[][] results = charsArray(secret, guess);
        int[] secretArray = results[0];
        int[] guessArray = results[1];
        int[] bullsArray = results[2];

        int cows = 0;

        for (int index = 0; index < secretArray.length; index++) {
                cows += Math.min(secretArray[index], guessArray[index]);
        }

        String retValue = String.valueOf(bullsArray[0]) + "A" + String.valueOf(cows) + "B";

        return retValue;
    }

    private static int[][] charsArray(String secret, String guess) {
        int length = secret.length();
        int[] secretsArray = new int[10];
        int[] guessArray = new int[10];
        int[] bulls = new int[10];
        for (int index = 0; index < length; index++) {
            int left = secret.charAt(index) - 48;
            int right = guess.charAt(index) - 48;
            if (left == right) {
                bulls[0]++;
            } else {
                secretsArray[left]++;
                guessArray[right]++;
            }
        }


        int[][] retValue = new int[3][secret.length()];
        retValue[0] = secretsArray;
        retValue[1] = guessArray;
        retValue[2] = bulls;

        return retValue;
    }
}
