package Leetcode.Easy.Strings;

import java.util.ArrayList;
import java.util.Collections;

public class LC_443 {
    public static int compress(char[] chars) {
        int retValue = 0;
        if (chars == null || chars.length == 0) {
            return retValue;
        }

        if (chars.length == 1) {
            return 1;
        }

        int pointer = 0;
        int charsAmount = 1;
        char current = chars[0];

        for (int i = 1; i < chars.length; i++) {
            if (current != chars[i]) {
                if (charsAmount > 1) {
                    chars[pointer++] = current;
                    char[] digits = convertedDigit(charsAmount);
                    for (int j = 0; j < digits.length; j++) {
                        chars[pointer++] = digits[j];
                    }
                } else {
                    chars[pointer] = current;
                    ++pointer;
                }
                current = chars[i];
                charsAmount = 1;
            } else {
                charsAmount++;
            }
        }

        chars[pointer++] = current;

        if (charsAmount > 1) {
            char[] digits = convertedDigit(charsAmount);
            for (int j = 0; j < digits.length; j++) {
                chars[pointer++] = digits[j];
            }
        }

        return pointer;
    }

    private static char[] convertedDigit(int digit) {
        ArrayList<Integer> list = new ArrayList<>();
        while (digit > 0) {
            list.add(digit % 10);
            digit = digit / 10;
        }

        Collections.reverse(list);
        int size = list.size();
        char[] retValue = new char[size];
        for (int i = 0; i < size; i++) {
            int asciiValue = list.get(i) + 48;
            retValue[i] = (char)asciiValue;
        }

        return retValue;
    }
}
