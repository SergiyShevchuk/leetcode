package Leetcode.Easy.Strings;

/* 383. Ransom Note */

public class LC_383 {
    public static boolean canConstruct(String ransomNote, String magazine) {
        if (ransomNote == null && magazine == null) {
            return true;
        }

        if (ransomNote == null || magazine == null) {
            return false;
        }

        if (ransomNote.length() == 0) {
            return true;
        }

        char[] ransomStorage = new char[26];
        char[] magazineStorage = new char[26];

        for (int index = 0; index < ransomNote.length(); index++) {
            ransomStorage[ransomNote.charAt(index) - 'a']++;
        }

        for (int index = 0; index < magazine.length(); index++) {
            magazineStorage[magazine.charAt(index) - 'a']++;
        }

        for (int index = 0; index < 26; index++) {
            if (ransomStorage[index] <= magazineStorage[index]) {
             continue;
            } else {
                return false;
            }
        }

        return true;
    }
}
