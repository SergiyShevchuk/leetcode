package Leetcode.Easy.Strings;

import java.util.*;

public class LC_819 {
    public static String mostCommonWord(String paragraph, String[] banned) {
        if (paragraph == null || banned == null) {
            return null;
        }

        Queue<String> wordsQueue = new LinkedList<>();
        HashMap<String, Integer> storage = new HashMap<>();

        char[] validChars = new char[256];
        for (int index = 0; index < validChars.length; index++) {
            if ((index >= 65 && index <= 90) || (index >= 97 && index <= 122)) {
                validChars[index]++;
            }

        }

        String buider = "";
        char[] charsArray = paragraph.toCharArray();
        for (int index = 0; index < charsArray.length; index++) {
            Character curr = Character.toLowerCase(charsArray[index]);
            if (validChars[curr] == 1 && buider.length() >= 0) {
                buider = buider + curr;
            } else {
                if (buider.length() >= 1) {
                    if (!storage.containsKey(buider)) {
                        wordsQueue.add(buider);
                    }
                    Integer counter = storage.getOrDefault(buider, 0);
                    storage.put(buider, counter + 1);
                }
                buider = "";
            }

            if (index == charsArray.length - 1 && buider.length() >= 1) {
                if (!storage.containsKey(buider)) {
                    wordsQueue.add(buider);
                }
                Integer counter = storage.getOrDefault(buider, 0);
                storage.put(buider, counter + 1);
            }
        }

        validChars = null;

        for (String bannedWord: banned) {
            String keyWord = bannedWord.toLowerCase();
            if (storage.containsKey(keyWord)) {
                storage.remove(keyWord);
            }
        }

        TreeSet<Integer> popIndexes = new TreeSet<>();

        for (String popWord: storage.keySet()) {
            popIndexes.add(storage.get(popWord));
        }

        if (banned.length == 0) {
            String word = wordsQueue.poll();
            return word;
        }

        String retValue = null;
        Set<String> popWords = new HashSet<>();
        Integer lastIndex = popIndexes.last();

        for (String popWord: storage.keySet()) {
            if (lastIndex == storage.get(popWord)) {
                popWords.add(popWord);
            }
        }

        storage = null;

        while (!wordsQueue.isEmpty()) {
            String word = wordsQueue.poll();
            if (popWords.contains(word)) {
                return word;
            }
        }

        return retValue;
    }
}
