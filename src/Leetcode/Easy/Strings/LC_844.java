package Leetcode.Easy.Strings;
import java.util.*;

public class LC_844 {
    public boolean backspaceCompare(String S, String T) {
        Deque<Character> left = processText(S);
        Deque<Character> right = processText(T);

        if (left.size() != right.size()) { return false; }

        while (!left.isEmpty()) {
            if (left.pollLast() != right.pollLast()) {
                return false;
            }
        }

        return true;
    }

    private Deque<Character> processText(String text) {
        Deque<Character> stack = new LinkedList<>();

        for (int i = 0; i < text.length(); i++) {
            Character curr = text.charAt(i);
            if (curr == '#' && !stack.isEmpty()) {
                stack.pollLast();
            } else if (curr != '#') {
                stack.add(curr);

            }
        }

        return stack;
    }
}
