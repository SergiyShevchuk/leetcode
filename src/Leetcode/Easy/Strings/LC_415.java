package Leetcode.Easy.Strings;

import java.util.Arrays;
import java.util.Collections;

public class LC_415 {
    public static String addStrings(String num1, String num2) {
        StringBuilder builder = new StringBuilder();

        int i = num1.length() - 1;
        int j = num2.length() - 1;
        int diff = 0;

        while (i >= 0 && j >= 0) {
            int summ = conversion(num1, i) + conversion(num2, j);
            if (summ + diff >= 10) {
                int recDigit = ((summ + diff) % 10) + 48;
                diff = 1;
                char recChar = (char)recDigit;
                builder.append(recChar);
            } else {
                char recChar = (char)(summ + diff + 48);
                builder.append(recChar);
                diff = 0;
            }
            i--;
            j--;
        }

        if (j >= 0) {
            convertString(num2, j, builder, diff);
            diff = 0;
        } else if (i >= 0) {
            convertString(num1, i, builder, diff);
            diff = 0;
        } else if (diff == 1) {
            char last = (char)(1 + 48);
            builder.append(last);
        }
        return builder.reverse().toString();
    }

    private static void convertString(String s, int index, StringBuilder builder, int diff) {
        for (int i = index; i >= 0; i--) {
            char current = s.charAt(i);
            int summ = current - 48;
            if (summ + diff == 10) {
                diff = 1;
                char recChar = (char)(48);
                builder.append(recChar);
            } else {
                char recChar = (char)(summ + diff + 48);
                builder.append(recChar);
                diff = 0;
            }
        }

        if (diff == 1) {
            char last = (char)(1 + 48);
            builder.append(last);
        }
    }

    private static int conversion(String s, int index) {
        char current = s.charAt(index);
        int currInt = current - 48;
        return currInt;
    }
}
