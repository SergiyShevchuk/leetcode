package Leetcode.Easy.Strings;

import java.util.ArrayList;
import java.util.HashMap;

public class LC_657 {
    public static boolean judgeCircle(String moves) {

        if (moves == null || moves.length() <= 1) {
            return false;
        }

        int[] coordinates = {0, 0};

        for (int i = 0; i < moves.length(); i++) {
            char moveDirection = moves.charAt(i);
            if (moveDirection == 'U') {
                coordinates[1] += 1;
            } else if (moveDirection == 'D') {
                coordinates[1] -= 1;
            } else if (moveDirection == 'L') {
                coordinates[0] -= 1;
            } else if (moveDirection == 'R') {
                coordinates[0] += 1;
            }
        }

        return coordinates[0] == 0 && coordinates[1] == 0;
    }
}
