package Leetcode.Easy.Strings;

public class LC_389 {
    public static char findTheDifference(String s, String t) {
        char[] storage = new char[26];

        for (int index = 0; index < s.length(); index++) {
            char current = s.charAt(index);
            storage[current - 'a']++;
        }

        for (int index = 0; index < t.length(); index++) {
            char current = t.charAt(index);
            storage[current - 'a']--;
        }

        for (int index = 0; index < storage.length; index++) {
            if (storage[index] != 0) {
               index = index + 97;
               char retValue = (char)index;
               return retValue;
            }
        }

        return ' ';
    }
}
