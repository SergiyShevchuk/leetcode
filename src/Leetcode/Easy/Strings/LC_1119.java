package Leetcode.Easy.Strings;

import java.util.HashSet;
import java.util.Set;

public class LC_1119 {
    public String removeVowels(String S) {

        Set<Character> volves = new HashSet<>();
        char[] _volves = {'a', 'e', 'i', 'o', 'u'};
        for (char curr: _volves) {
            volves.add(curr);
        }

        StringBuilder buider = new StringBuilder();

        for (char currenr: S.toCharArray()) {
           if (!volves.contains(currenr)) {
               buider.append(currenr);
           }
        }

        return buider.toString();
    }
}
