package Leetcode.Easy.Strings;

public class LC_344 {
    public static void reverseString(char[] s) {
        if (s == null || s.length == 0 || s.length == 1) {
            return;
        }

        int mid = s.length / 2;
        int length = s.length;
        for (int index = 0; index < mid; index++) {
            char last = s[length - 1 - index];
            s[length - 1 - index] = s[index];
            s[index] = last;
        }

        return;
    }
}
