package Leetcode.Easy.Strings;

/* Decrypt String from Alphabet to Integer Mapping

Given a string s formed by digits ('0' - '9') and '#' . We want to map s to English lowercase characters as follows:

Characters ('a' to 'i') are represented by ('1' to '9') respectively.
Characters ('j' to 'z') are represented by ('10#' to '26#') respectively.
Return the string formed after mapping.

It's guaranteed that a unique mapping will always exist.
 */

public class LC_1309 {
    public static String freqAlphabets(String s) {
        StringBuilder retValue = new StringBuilder();

        int sLen = s.length();

        for (int index = 0; index < sLen; index++) {

          if (index + 2 < sLen && s.charAt(index + 2) == '#') {
              String left = s.substring(index, index + 2);
            Integer value = Integer.valueOf(left);
            char letter = (char)(97 + value - 1);
            retValue.append(letter);
            index += 2;
          } else if (s.charAt(index) != '#'){
              String left = String.valueOf(s.charAt(index));
              char letter = (char)(97 + Integer.valueOf(left) - 1);
              retValue.append(letter);
          }
        }

        return retValue.toString();
    }
}
