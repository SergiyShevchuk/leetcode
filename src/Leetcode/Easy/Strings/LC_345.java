package Leetcode.Easy.Strings;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class LC_345 {
    public static String reverseVowels(String s) {

        Set<Character> volves = new HashSet<>();
        char[] _volves = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
        for (char curr: _volves) {
            volves.add(curr);
        }

        int left = 0;
        int right = s.length() - 1;
        char[] word = s.toCharArray();

        while (left < right) {
            if (volves.contains(word[left]) == false) {
                left++;
            } else if (volves.contains(word[right]) == false) {
                right--;
            } else {
                char tmp = s.charAt(left);
                word[left] = word[right];
                word[right] = tmp;
                right--;
                left++;
            }
        }

        return String.valueOf(word);
    }
}
