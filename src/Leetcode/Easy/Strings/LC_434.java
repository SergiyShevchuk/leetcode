package Leetcode.Easy.Strings;

public class LC_434 {
    public static int countSegments(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        int segments = 0;
        String[] segmentsArray = s.split(" ");
        for (int index = 0; index < segmentsArray.length; index++) {
            if (segmentsArray[index].length() > 0) {
                segments++;
            }
        }

        return segments;
    }
}
