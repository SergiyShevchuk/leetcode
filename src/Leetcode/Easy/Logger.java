package Leetcode.Easy;

import java.util.*;

public class Logger {
    Map<String, Integer> storage;

    /** Initialize your data structure here. */
    public Logger() {
        this.storage = new HashMap<>();
    }

    /** Returns true if the message should be printed in the given timestamp,
     * otherwise returns false.
     If this method returns false, the message will not be printed.
     The timestamp is in seconds granularity. */

    public boolean shouldPrintMessage(int timestamp, String message) {
        if (storage.containsKey(message)) {
            int diff = timestamp - storage.get(message);
            if (diff > 10) {
                storage.put(message, timestamp);
            }
            return diff > 10;
        } else {
            storage.put(message, timestamp);
            return true;
        }
    }
}
