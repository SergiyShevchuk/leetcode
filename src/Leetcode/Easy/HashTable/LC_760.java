package Leetcode.Easy.HashTable;

import java.util.HashMap;

public class LC_760 {
    public static int[] anagramMappings(int[] A, int[] B) {
        if (A == null || B == null) {
            return null;
        }

        if (A.length != B.length) {
            return null;
        }

        HashMap<Integer, Integer> storage = new HashMap<>();

        for (int index = 0; index < A.length; index++) {
          Integer value = B[index];
          storage.put(value, index);
        }

        int[] retValue = new int[A.length];

        for (int index = 0; index < A.length; index++) {
            Integer value = A[index];
            retValue[index] = storage.get(value);
        }

        return retValue;
    }
}
