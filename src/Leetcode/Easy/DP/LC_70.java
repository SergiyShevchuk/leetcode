package Leetcode.Easy.DP;

public class LC_70 {
    public static int climbStairs(int n) {
        if (n <= 1) {
            return 1;
        }

        int[] storage = new int[n + 1];
        storage[1] = 1;
        storage[2] = 2;

        for (int index = 3; index <= n; index++) {
            storage[index] = storage[index - 1] + storage[index - 2];
        }

        int retValue = storage[n];

        return retValue;
    }


    // For small n values <= 20
    private static int auxiliaryClimbingResursive(int n) {
        if (n <= 1) {
            return 1;
        }

        return auxiliaryClimbingResursive(n - 1) + auxiliaryClimbingResursive(n - 2);
    }
}
