package Leetcode.Easy.DP;

/* House Robber */

public class LC_198 {
    public static int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        if (nums.length == 1) {
            return nums[0];
        }

        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }

        int maxEven = 0;
        int maxOdd = 0;


        for (int index = 0; index < nums.length; index++) {
            if (index + 1 < nums.length) {
                maxOdd += nums[index + 1];
            }
            maxEven += nums[index];
            index++;
        }

        int maxProfix = Math.max(maxEven, maxOdd);
        return maxProfix;
    }
}
