package Leetcode.Easy.Math;

/*
1134. Armstrong Number
 */

import java.util.ArrayList;
import java.util.Stack;

public class LC_1134 {
    public static boolean isArmstrong(int N) {
        Stack<Integer> storage = reverseDigit(N);
        int pow = storage.size();
        double refValue = 0;
        while (!storage.isEmpty()) {
            refValue += Math.pow(storage.pop(), pow);
        }

        int value = (int)refValue;

        return value == N;
    }

    private static Stack<Integer> reverseDigit(int N) {
        Stack<Integer> storage = new Stack<>();

        while (N != 0) {
            storage.add( N % 10);
            N = N / 10;
        }

        return storage;
    }
}
