package Leetcode.Easy.Math;

public class LC_1342 {
    public static int numberOfSteps (int num) {
        if (num == 0) { return 0; }
        if (num == 1) { return 1; }

        int stesp = 0;
        while (num > 0) {
            if(num%2==0) {
                num/=2;
            } else {
                num--;
            }
            stesp++;
        }

        return stesp;
    }
}
