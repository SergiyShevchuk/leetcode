package Leetcode.Easy.Common;

public class LC_292 {
    public static boolean canWinNim(int n) {
        if (n <= 3) {
            return true;
        }

        return (n % 4 != 0);
    }
}
