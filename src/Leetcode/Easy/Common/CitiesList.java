package Leetcode.Easy.Common;

/*
"Me,Amsterdam,Barcelona,London,Prague"

U1,Amsterdam,Barcelona,London,Prague
U2,Shanghai,Hong Kong,Moscow,Sydney,Melbourne
U3,London,Boston,Amsterdam,Madrid
U4,Barcelona,Prague,London,Sydney,Moscow"

travel buddy is a user who has 50% or more cities in common with you

output
U1
U4
U3

input: string me, string users

output: list of strings
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CitiesList {


    public static ArrayList<String> commonCities(String myCities, String fellaCities) {
        ArrayList<String> storage = new ArrayList<>();

        if (myCities == null || myCities.length() == 0) {
            return null;
        }

        String[] myCitiesArray = myCities.split(",");
        Set<String> myStorage = new HashSet<>();
        for (int index = 1; index < myCitiesArray.length; index++) {
            myStorage.add(myCitiesArray[index]);
        }

        String[] splitedCities = fellaCities.split(" ");
        String retValue = "";

        for (int index = 0; index < splitedCities.length; index++) {
            String currentFella = intersectedCities(splitedCities[index], myStorage);
            if (currentFella != null) {
                storage.add(currentFella);
            }
        }

        return storage;
    }

    private static String intersectedCities(String fellaCities, Set<String> myCities) {
        String[] citiesArray = fellaCities.split(",");
        String fellaName = citiesArray[0];

        int intersections = 0;
        for (int index = 1; index < citiesArray.length; index++) {
            if (myCities.contains(citiesArray[index])) {
                intersections++;
            }
        }

        if (intersections >= citiesArray.length / 2) {
            return fellaName;
        }

        return null;
    }
}
