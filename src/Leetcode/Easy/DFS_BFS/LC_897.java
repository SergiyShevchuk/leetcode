package Leetcode.Easy.DFS_BFS;

import Common.DataStructures.TreeNode;

import java.util.ArrayList;

public class LC_897 {
    public static TreeNode increasingBST(TreeNode root) {

        if (root == null || (root.left == null && root.right == null)) {
            return root;
        }

        ArrayList<Integer> list =  new ArrayList<>();
        traverse(root, list);

        TreeNode ans = new TreeNode(list.get(0));
        TreeNode curr = ans;

        for (int i = 1; i < list.size(); i++) {
            curr.right = new TreeNode(list.get(i)) ;
            curr.left = null;
            curr = curr.right;
        }

        return ans;
    }

    private static void traverse(TreeNode root, ArrayList<Integer> list) {
        if (root == null) { return; }
        traverse(root.left, list);
        list.add(root.val);
        traverse(root.right, list);
    }
}
