package Leetcode.Easy.DFS_BFS;

import java.util.*;

public class LC_994 {

    static int[][] directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    public static int orangesRotting(int[][] grid) {

        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return -1;
        }

        int cellsAmount = grid.length * grid[0].length;
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        int rotten = 0;
        int empty = 0;
        int fresh = 0;

        ArrayList<Queue<Integer>> rottenPoints = new ArrayList<>();

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                int state = grid[i][j];
                visited[i][j] = true;
                if (state == 0) {
                    empty++;
                } else if (state == 1) {
                    fresh++;
                    visited[i][j] = false;
                } else if (state == 2) {
                    Queue<Integer> point = new LinkedList<>();
                    point.add(i);
                    point.add(j);
                    rottenPoints.add(point);
                    rotten++;
                }
            }
        }

        if (rotten == cellsAmount || empty == cellsAmount) { return 0; }
        if (fresh == cellsAmount) { return -1; }

        Queue<ArrayList<Queue<Integer>>> storage = new LinkedList<>();
        storage.add(rottenPoints);

        int minunes = 0;

        while (!storage.isEmpty()) {
            ArrayList<Queue<Integer>> layer = storage.poll();
            ArrayList<Queue<Integer>> newxtLayer = new ArrayList<>();
            for (Queue<Integer> layerPoint: layer) {
                int i = layerPoint.poll();
                int j = layerPoint.poll();
                for (int[] direction: directions) {
                    if (isMovePossible(grid, i + direction[0], j + direction[1], visited)) {
                        Queue<Integer> nextStep = new LinkedList<>();
                        nextStep.add(i + direction[0]);
                        nextStep.add(j + direction[1]);
                        newxtLayer.add(nextStep);
                    }
                }
            }
            if (newxtLayer.size() != 0) {
                minunes++;
                storage.add(newxtLayer);
            }
        }

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
              if (visited[i][j] == false) {
                  return -1;
              }
            }
        }

        return minunes;
    }

    private static boolean isMovePossible(int[][] grid, int i, int j, boolean[][] visited) {
        if (i >= grid.length ||
                i < 0 ||
                j >= grid[0].length ||
                j < 0 ||
                grid[i][j] == 2 ||
                grid[i][j] == 0 ||
                visited[i][j] == true) {
            return false;
        }

        visited[i][j] = true;
        return true;
    }
}
