package Leetcode.Easy.Array;

import java.util.HashSet;
import java.util.Set;

public class LC_136 {
    public int singleNumber(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        Set<Integer> storage = new HashSet<>();
        int retValue = 0;

        for (Integer value: nums) {
            if (storage.contains(value)) {
                storage.remove(value);
                retValue -= value;
            } else {
                storage.add(value);
                retValue += value;
            }
        }

        return retValue;
    }
}
