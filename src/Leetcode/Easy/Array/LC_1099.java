package Leetcode.Easy.Array;

/* Two Sum Less Than K */

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

public class LC_1099 {
    public static int twoSumLessThanK(int[] A, int K) {
        Arrays.sort(A);
        int left = 0;
        int right = A.length - 1;
        int result = -1;

        while (left < right) {
            int sum = A[left] + A[right];
            if (sum < K) {
                result = Math.max(sum, result);
                left++;
            } else {
                right--;
            }
        }

        return result;
    }
}
