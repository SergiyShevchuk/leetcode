package Leetcode.Easy.Array;

public class LC_169 {
    public int majorityElement(int[] nums) {
        int summ = 0;
        Integer element = null;

        for (Integer value: nums) {
            if (summ == 0) {
                element = value;
            }

            summ += element == value ? 1 : -1;
        }

        return element;
    }
}
