package Leetcode.Easy.Array;

/*
1317. Convert Integer to the Sum of Two No-Zero Integers
Given an integer n. No-Zero integer is a positive integer which doesn't
contain any 0 in its decimal representation.

Return a list of two integers [A, B] where:

A and B are No-Zero integers.
A + B = n

It's guarateed that there is at least one valid solution.
If there are many valid solutions you can return any of them.
 */

public class LC_1317 {
    public static int[] getNoZeroIntegers(int n) {
        int[] retValue = new int[2];

        boolean flag = false;
        int right = 1;

        while (flag != true) {
            int left = n - right;
            boolean rightB = checkZero(right);
            boolean leftB = checkZero(left);
            if (leftB == true && rightB == true) {
                retValue[0] = right;
                retValue[1] = left;
                flag = true;
            }
            right++;
        }

        return retValue;
    }

    public static boolean checkZero(int n) {

        while (n != 0) {
            int rest = n % 10;
            if (rest == 0) {
                return false;
            }
            n = n / 10;
        }

        return true;
    }
}
