package Leetcode.Easy.Array;

public class LC_1085 {
    public int sumOfDigits(int[] A) {

        int min = A[0];
        for (int i = 1; i < A.length; i++) {
            min = Math.min(min, A[i]);
        }

        int result = 0;

        while (min > 0) {
            result += min % 10;
            min = min / 10;
        }

        result = result % 2 == 0 ? 1 : 0;

        return result;
    }


}
