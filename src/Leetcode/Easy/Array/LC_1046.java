package Leetcode.Easy.Array;

import java.util.Collections;
import java.util.PriorityQueue;

public class LC_1046 {
    public static int lastStoneWeight(int[] stones) {
        int lastWeight = 0;
        if (stones.length == 0) { return lastWeight; }
        if (stones.length == 1) { return stones[0]; }

        int[] weigts = new int[1001];
        for (Integer value: stones) {
            weigts[value]++;
        }

        boolean flag = true;
        int pointer = weigts.length - 1;

        for (int i = weigts.length - 1; i >= 1; i--) {
            if (weigts[i] % 2 != 0) {
                for (int j = i - 1; j >= 1; j--) {
                    if (weigts[j] >= 1) {
                        int diff = i - j;
                        weigts[j]--;
                        weigts[i] = 0;
                        weigts[diff]++;
                        break;
                    }
                }
            } else {
                weigts[i] = 0;
            }
        }

        for (int i = 1; i < weigts.length; i++) {
            if (weigts[i] >= 1) {
                return weigts[i] % 2 == 0 ? 0 : i;
            }
        }

        return 0;
    }
}
