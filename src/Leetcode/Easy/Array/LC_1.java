package Leetcode.Easy.Array;

import java.util.HashSet;
import java.util.Set;

public class LC_1 {

    // Hashable approach
    public int[] twoSum(int[] nums, int target) {
        if (nums == null) {
            return null;
        }

        if (nums.length == 0) {
            return null;
        }

        int[] retValue = new int[2];

        if (nums.length == 1 && nums[0] == target) {
            return retValue;
        }

        Set<Integer> storage = new HashSet<>();

        for (Integer value: nums) {
            storage.add(value);
        }

        for (int index = 0; index < nums.length; index++) {
            int diff = target - nums[index];
            if (storage.contains(diff)) {
                retValue[0] = index;
                for (int inner = index + 1; inner < nums.length; inner++) {
                    if (nums[inner] == diff) {
                        retValue[1] = inner;
                        return retValue;
                    }
                }
            }
        }

        return null;
    }

    // Two pointers approach approach
    public int[] twoSumPointers(int[] nums, int target) {
        if (nums == null) {
            return null;
        }

        if (nums.length == 0) {
            return null;
        }

        int[] retValue = new int[2];

        if (nums.length == 1 && nums[0] == target) {
            return retValue;
        }

        int left = 0;
        int right = nums.length - 1;

        while (left < right) {
            if (nums[left] + nums[right] == target) {
                retValue[0] = left;
                retValue[1] = right;
            } else if (nums[left] + nums[right] > target) {
                left++;
            } else {
                right--;
            }
        }

        return null;
    }
}
