package Leetcode.Easy.Array;

/*
    1313. Decompress Run-Length Encoded List
 */

import java.util.ArrayList;

public class LC_1313 {
    public static int[] decompressRLElist(int[] nums) {
        ArrayList<Integer> storage = new ArrayList<>();

        for (int index = 0; index < nums.length; index++) {
          int value = nums[index + 1];
              int pointer = nums[index];
              while (pointer != 0) {
                  storage.add(value);
                  pointer--;
              }
          index++;
        }

        int[] retValue = new int[storage.size()];

        for (int index = 0; index < retValue.length; index++) {
            retValue[index] = storage.get(index);
        }

        storage = null;

        return retValue;
    }
}
