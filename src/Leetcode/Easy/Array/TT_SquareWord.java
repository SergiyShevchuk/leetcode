package Leetcode.Easy.Array;
import java.util.*;

public class TT_SquareWord {
    public static void DFSPath(String inputWord, String s) {

        if (s == null || s.length() == 0 || inputWord == null || inputWord.length() <= 3) {
            return;
        }
        int mSize = (int)Math.sqrt(inputWord.length());
        if (Math.pow((double)mSize, 2) != inputWord.length()) {
            return;
        }

        int pointer = 0;
        Queue<ArrayList<Integer>> startPoints = new LinkedList<>();
        char startLetter = s.charAt(pointer);
        char[][] storage = new char[mSize][mSize];

        for (int index = 0; index < mSize; index++) {
            for (int internalIndex = 0; internalIndex < mSize; internalIndex++) {
                char current = inputWord.charAt(pointer++);
                storage[index][internalIndex] = current;
                if (current == startLetter) {
                    ArrayList<Integer> coordinates = new ArrayList<>();
                    coordinates.add(index);
                    coordinates.add(internalIndex);
                    startPoints.add(coordinates);
                }
            }
        }

        for (int index = 0; index < startPoints.size(); index++) {
            ArrayList<Integer> node = startPoints.poll();
            boolean[][] visited = new boolean[mSize][mSize];
            Stack<ArrayList<Integer>> path = new Stack<>();
            if (DFSSearch(storage, s, node.get(0), node.get(1), 0, visited, path)) {
                printPath(path);
            }
        }
    }

    private static boolean DFSSearch(char[][] storage,
                                     String word,
                                     int i,
                                     int j,
                                     int pointer,
                                     boolean[][] visited,
                                     Stack<ArrayList<Integer>> path) {
        if (pointer == word.length()) {
            return true;
        }

        int sLength = storage.length;

        if(i >= sLength ||
                i < 0 ||
                j >= sLength ||
                j < 0 ||
                storage[i][j] != word.charAt(pointer) ||
                visited[i][j]){
            return false;
        }

        visited[i][j] = true;
        ArrayList<Integer> node = new ArrayList<>();
        node.add(i);
        node.add(j);
        path.add(node);

        pointer += 1;

        if (DFSSearch(storage, word, i - 1, j, pointer, visited, path) ||
                DFSSearch(storage, word, i + 1, j, pointer, visited, path) ||
                DFSSearch(storage, word, i, j + 1, pointer, visited, path) ||
                DFSSearch(storage, word, i, j - 1, pointer, visited, path)) {
            return true;
        }

        path.pop();
        visited[i][j] = false;

     return false;
    }
    
    private static void printPath(Stack<ArrayList<Integer>> path) {
        for (ArrayList<Integer> point: path) {
            System.out.println(point);
        }
    }
}
