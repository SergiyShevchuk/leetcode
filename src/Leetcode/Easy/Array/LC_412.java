package Leetcode.Easy.Array;

import java.util.LinkedList;
import java.util.List;

public class LC_412 {
    public static List<String> fizzBuzz(int n) {

        if (n == 0) {
            return null;
        }

        List<String> storage = new LinkedList<>();

        int index = 0;

        while (index != n) {
            index++;

            String builder = "";

            Boolean isThree = index % 3 != 0;
            Boolean isFive = index % 5 != 0;

            if (isThree == true && isFive == true) {
                builder = String.valueOf(index);
            } else {
                if (isThree == false) {
                    builder += "Fizz";
                }

                if (isFive == false) {
                    builder += "Buzz";
                }
            }

            storage.add(builder);
            builder = "";
        }

        return storage;
    }
}
