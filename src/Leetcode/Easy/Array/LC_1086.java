package Leetcode.Easy.Array;

import java.util.*;

public class LC_1086 {
    public static int[][] highFive(int[][] items) {
        HashMap<Integer, ArrayList<Integer>> storage = new HashMap<>();

        for (int[] item: items) {
            ArrayList<Integer> scores = storage.getOrDefault(item[0], new ArrayList<>());
            scores.add(item[1]);
            storage.put(item[0], scores);
        }

        TreeSet<Integer> students = new TreeSet<>();
        students.addAll(storage.keySet());
        int[][] retValue = new int[students.size()][2];

        for (int i = 0; i < retValue.length; i++) {
           int key = students.first();
           int average = computeAverage(storage.get(key));
           int[] aa = {key, average};
           retValue[i] = aa;
           students.remove(key);
        }

        return retValue;
    }

    private static int computeAverage(ArrayList<Integer> scoreList) {
        int max = 0;
        Collections.sort(scoreList);
        if (scoreList.size() > 5) {
            for (int i = scoreList.size() - 1; i > scoreList.size() - 1 - 5; i--) {
                max += scoreList.get(i);
            }

            return max / 5;
        }

        for (int score: scoreList) {
            max += score;
        }

        return max / scoreList.size();
    }
}
