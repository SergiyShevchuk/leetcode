package Leetcode.Easy.Array;

public class LC_283 {
    public static void moveZeroes(int[] nums) {
        if (nums == null || nums.length <= 1) {
            return;
        }
        int leftP = 0;
        for (int index = 0; index < nums.length; index++) {
            if (nums[index] != 0) {
                nums[leftP] = nums[index];
                leftP++;
            }
        }

        for (int index = leftP; index < nums.length; index++) {
            nums[index] = 0;
        }
    }
}
