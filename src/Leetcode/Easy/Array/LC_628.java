package Leetcode.Easy.Array;

import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class LC_628 {
    public int maximumProduct(int[] nums) {

        PriorityQueue<Integer> maxHeap = new PriorityQueue<>();
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();

        for (Integer value: nums) {

            if (value > 0) {
                maxHeap.add(value);
                if (maxHeap.size() > 3) maxHeap.poll();
            } else {
                minHeap.add(value);
                if (minHeap.size() > 2) {
                    Deque<Integer> deque = new LinkedList<>();
                    for (int i = 0; i < 2; i++) {
                        deque.add(minHeap.poll());
                    }
                    minHeap.clear();
                    while (!deque.isEmpty()) {
                        minHeap.add(deque.pollFirst());
                    }
                }
            }

        }

        int retValue = 0;
        if (minHeap.size() == 2) {
            int max1 = maxHeap.poll();
            int max2 = maxHeap.poll();
            int max3 = maxHeap.poll();
            retValue = Math.max(minHeap.poll() * minHeap.poll() * max3, max1 * max2 * max3);
        }

        return retValue;
    }
}
