package Leetcode.Easy.Array;

public class LC_66 {
    public static int[] plusOne(int[] digits) {
        int length = digits.length;
        if (digits[length - 1] != 9) {
            digits[length - 1] += 1;
            return digits;
        }

        int left = 0;

        while (left < length) {
            if (digits[length - 1 - left] != 9) {
                digits[length - 1 - left] += 1;
                return digits;
            } else {
                digits[length - 1 - left] = 0;
                left++;
            }
        }

        int[] newDigits = new int[length + 1];
        newDigits[0] = 1;
        for (int i = 0; i < length; i++) {
            newDigits[i + 1] = digits[i];
        }
        return newDigits;
    }
}
