package Leetcode.Easy.Array;

public class LC_189 {
    public void rotate(int[] nums, int k) {
        if (nums == null || nums.length <= 1 || k == 0) {
            return;
        }

        int lenght = nums.length;

        for (int index = 0; index < k; index++) {
            int last = nums[lenght - 1];
            for (int j = lenght - 2; j >= 0; j--) {
                nums[j + 1] = nums[j];
            }
            nums[0] = last;
        }
    }
}
