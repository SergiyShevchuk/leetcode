package Leetcode.Easy.Array;

/* Replace Elements with Greatest Element on Right Side */

public class LC_1299 {
    public static int[] replaceElements(int[] arr) {

        if (arr.length == 0) { return arr; }

        int len = arr.length - 1;
        int max = arr[len];

        for (int i = len - 2; i >= 0; i--) {
            int tmp = arr[i];
            arr[i] = max;
            max = Math.max(tmp, max);
        }

        arr[len] = -1;

        return arr;
    }
    /*
     int n = arr.length;
        if(n == 0) return arr;
        int max = arr[n - 1];
        for(int i = n - 2; i >= 0; i--){
            int temp = arr[i];
            arr[i] = max;
            max = Math.max(max, temp);
        }
        arr[n - 1] = -1;
        return arr;
     */

}
