package Leetcode.Easy.Array;

import java.util.*;

public class LC_937 {
    public static String[] reorderLogFiles(String[] logs) {
        if (logs == null || logs.length == 0) {
            return null;
        }

        String[] retValue = new String[1];

        if (logs.length == 1) {
            retValue[0] = logs[0];
            return retValue;
        }

        Queue<String> digitsStorage = new LinkedList<>();
        Queue<String> lettersStorage = new LinkedList<>();

        for (String logString: logs) {
            String[] split = logString.split(" ", 2);
            if (split[1].charAt(0) < 'a') {
                digitsStorage.add(logString);
            } else {
                lettersStorage.add(logString);
            }
        }

        return retValue;
    }
}
