package Leetcode.Easy.Array;

import java.util.HashSet;
import java.util.Set;

public class LC_349 {
    public int[] intersection(int[] nums1, int[] nums2) {

        int[] emptyValue = new int[0];

        if (nums1 == null || nums1.length == 0) {
            return nums2;
        }

        if (nums2 == null || nums2.length == 0) {
            return nums1;
        }

        emptyValue = null;

        Set<Integer> allValues = new HashSet<>();
        Set<Integer> storage = new HashSet<>();

        for (Integer value: nums1) {
          allValues.add(value);
        }

        for (Integer value: nums2) {
            if (allValues.contains(value)) {
                storage.add(value);
            }
        }

        allValues = null;

        Integer[] retValue = new Integer[storage.size()];
        int[] intStorage = new int[storage.size()];
        storage.toArray(retValue);
        storage = null;
        for (int index = 0; index < retValue.length; index++) {
            intStorage[index] = retValue[index].intValue();
        }

        return intStorage;
    }
}
