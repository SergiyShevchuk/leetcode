package Leetcode.Easy.Array;



public class LC_766 {
    public static boolean isToeplitzMatrix(int[][] matrix) {
        if (matrix.length == 0) { return true; }
        // {{1,2,3,4}, {5,1,2,3}, {9,5,1,2}};
        for (int i = 0; i < matrix.length - 1; i++) {
            // i = 0 // j = 0
            if (!isNextValueSame(matrix, i, 0, matrix[i][0])) {
                return false;
            }
        }

        for (int i = 1; i < matrix[0].length - 1; i++) {
            if (!isNextValueSame(matrix, 0, i, matrix[0][i])) {
                return false;
            }
        }

        return true;
    }

    private static boolean isNextValueSame(int[][] matrix, int i, int j, int value) {
        if ((i >= matrix.length || j >= matrix[i].length) && matrix[i - 1][j - 1] == value) {
            return true;
        } else if (matrix[i][j] != value) {
            return false;
        }

        return isNextValueSame(matrix, i + 1, j + 1, value);
    }
}
