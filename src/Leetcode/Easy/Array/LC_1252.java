package Leetcode.Easy.Array;

public class LC_1252 {
    public static int oddCells(int n, int m, int[][] indices) {
        int retValue = 0;

        if (n == 0 && m == 0) {
            return retValue;
        }

        if (indices == null || indices.length == 0) {
            return retValue;
        }

        int[][] storage = new int[n][m];

        for (int index = 0; index < indices.length; index++) {
            int[] indexValue = indices[index];
            arrayIncrementing(storage, indexValue);
        }

        for (int index = 0; index < storage.length; index++) {
            for (int inner = 0; inner < storage[index].length; inner++) {
                if (storage[index][inner] % 2 != 0) {
                    retValue++;
                }
            }
        }

        return retValue;
    }

    private static void arrayIncrementing(int[][] storage, int[] indices) {
        int[] row = storage[indices[0]];
        int collum = indices[1];

        for (int index = 0; index < row.length; index++) {
            row[index]++;
        }

        for (int index = 0; index < storage.length; index++) {
           storage[index][collum]++;
        }
    }
}
