package Leetcode.Easy.Array;

import java.util.LinkedList;
import java.util.List;

public class LC_448 {
    public static List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> ans = new LinkedList<>();

        // [4,3,2,7,8,2,3,1]

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != i + 1) {
                int position = nums[i];
                nums[position] = i + 1;
            }
        }

        return ans;
    }
}
