package Leetcode.Easy.Array;

import java.util.*;

public class LC_599 {
    public static String[] findRestaurant(String[] list1, String[] list2) {
        Map<String, Integer> storage = new HashMap<>();

        for (int i = 0; i < list1.length; i++) {
            storage.put(list1[i], i);
        }

        Map<String, Integer> intersections = new HashMap<>();
        int minIndex = Integer.MAX_VALUE;

        for (int i = 0; i < list2.length; i++) {
            String key = list2[i];
            if (storage.containsKey(key)) {
                int summ = storage.get(key) + i;
                minIndex = Math.min(summ, minIndex);
                intersections.put(key, summ);
            }
        }

        List<String> finalList = new ArrayList<>();
        for (String key: intersections.keySet()) {
            if (intersections.get(key) == minIndex) {
                finalList.add(key);
            }
        }

        return finalList.toArray(new String[finalList.size()]);
    }
}
