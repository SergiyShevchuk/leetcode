package Leetcode.Easy.Array;

public class LC_905 {
    public int[] sortArrayByParity(int[] A) {

        int ePointer = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] % 2 == 0) {
                int tmp = A[i];
                A[i] = A[ePointer];
                A[ePointer] = tmp;
                ePointer++;
            }
        }

        return A;
    }
}
