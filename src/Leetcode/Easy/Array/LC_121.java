package Leetcode.Easy.Array;

public class LC_121 {
    public static int maxProfit(int[] prices) {
        if (prices == null || prices.length <= 1) {
            return 0;
        }

        int maxProfit = 0;
        int minPrice = prices[0];

        for (int index = 1; index < prices.length; index++) {
            maxProfit = Math.max(maxProfit, prices[index] - minPrice);
            minPrice = Math.min(minPrice, prices[index]);
        }

        return maxProfit;
    }
}
