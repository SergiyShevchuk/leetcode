package Leetcode.Easy.Array;

public class LC_53 {
    public int maxSubArray(int[] nums) {
        if (nums.length == 0) { return 0; }
        if (nums.length == 1) { return nums[0]; }

        int maxValue = nums[0];
        int currSumm = nums[0];

        for (int i = 1; i < nums.length; i++) {
            currSumm = Math.max(nums[i], currSumm + nums[i]);
            maxValue = Math.max(maxValue, currSumm);
        }

        return maxValue;
    }
}
