package Leetcode.Contests;

import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class LC_W2_MinStack {

    private PriorityQueue<Integer> prQueue;
    private Deque<Integer> deque;

    public LC_W2_MinStack() {
        this.prQueue = new PriorityQueue<>();
        this.deque = new LinkedList<>();
    }

    public void push(int x) {
        prQueue.add(x);
        deque.add(x);
    }

    public void pop() {
        prQueue.remove(deque.pollLast());
    }

    public int top() {
        return deque.peekLast();
    }

    public int getMin() {
        return prQueue.peek();
    }
}
