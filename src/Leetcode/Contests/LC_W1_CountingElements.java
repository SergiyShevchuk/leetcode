package Leetcode.Contests;

import java.util.*;

public class LC_W1_CountingElements {
    public static int countElements(int[] arr) {
        int counter = 0;
        if (arr.length <= 1) {
            return counter;
        }

        Map<Integer, Integer> map = new HashMap<>();

        for (Integer key: arr) {
           Integer value = map.getOrDefault(key, 0);
           map.put(key, value + 1);
        }

        for (Integer key: map.keySet()) {
            if (map.containsKey(key + 1)) {
                counter += map.get(key);
            }
        }

        return counter;
    }
}
