package Leetcode.Contests;

import java.util.HashMap;

public class BiWC_15 {

    /*
    5127. Remove Covered Intervals
    Difficulty:Medium
    Given a list of intervals, remove all intervals that are
    covered by another interval in the list. Interval [a,b) is covered
    by interval [c,d) if and only if c <= a and b <= d.

    After doing so, return the number of remaining intervals.

    Example 1:
    Input: intervals = [[1,4],[3,6],[2,8]]
    Output: 2
    Explanation: Interval [3,6] is covered by [2,8], therefore it is removed.


    Constraints:

    1 <= intervals.length <= 1000
    0 <= intervals[i][0] < intervals[i][1] <= 10^5
    intervals[i] != intervals[j] for all i != j
     */

    public static int removeCoveredIntervals(int[][] intervals) {
        if (intervals == null || intervals.length <= 1) {
            return 0;
        }

        int covered = 0;

        for (int index = 0; index < intervals.length; index++) {
            int[] current = intervals[index];

            for (int inner = 0; inner < intervals.length; inner++) {
                if (index != inner) {
                    int[] next = intervals[inner];
                    if (next[0] <= current[0] && current[1] <= next[1]) {
                        covered++;
                        break;
                    }
                }
            }
        }

        covered = intervals.length - covered;

        return covered;
    }

    /*
    5126. Element Appearing More Than 25% In Sorted Array
    Difficulty:Easy
    Given an integer array sorted in non-decreasing order,
    there is exactly one integer in the array that occurs more than 25% of the time.
    Return that integer.

    Example 1:

    Input: arr = [1,2,2,6,6,6,6,7,10]
    Output: 6
     */

    public static int findSpecialIntegerOnePass(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        if (arr.length == 1 || arr.length == 2) {
            return arr[0];
        }

        int value = 0;
        int counter = 0;

        if (arr.length <= 4) {
            for (int index = 0; index < arr.length; index++) {
                if (counter >= 2) {
                    return value;
                } else {
                    if (value != arr[index]) {
                        value = arr[index];
                        counter = 1;
                    } else {
                        counter++;
                    }
                }
            }
        }

        Integer razer = arr.length / 4;
            for (int index = 0; index < arr.length; index++) {
                if (counter > razer) {
                    return value;
                } else {
                    if (value != arr[index]) {
                        value = arr[index];
                        counter = 1;
                    } else {
                        counter++;
                    }
                }
            }


        return 0;
    }

    public static int findSpecialInteger(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        if (arr.length == 1 || arr.length == 2) {
            return arr[1];
        }

        HashMap<Integer, Integer> storage = new HashMap<>();

        if (arr.length <= 4) {
            for (Integer key: storage.keySet()) {
                Integer currValue = storage.get(key);
                if (currValue >= 2) {
                    return key;
                }
            }
        }

        Integer razer = arr.length / 4;

        for (Integer key: storage.keySet()) {
            Integer currValue = storage.get(key);
            if (currValue > razer) {
                return key;
            }
        }

        return 0;
    }
}
