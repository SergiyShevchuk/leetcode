package com.company.FireCode;

import Common.DataStructures.ListNode;
import Common.DataStructures.TreeNode;

import java.util.ArrayList;

public class FireCode {

    public ListNode reverseList(ListNode head) {

        ListNode curr = null;
        ListNode next = head;

        // 1 2 3 4

        // 2

        while (next != null) {
            ListNode tmp = next.next;
            next.next = curr;
            curr = next;
            next = tmp;
        }

        return next;
    }
}
