package com.company.FireCode;

import java.util.Stack;

public class StackSorting {

    public static Stack<Integer> sortedStack(Stack<Integer> inputStack) {
        Stack<Integer> retValue = new Stack<>();
        if (inputStack == null || inputStack.isEmpty()) { return retValue; }

        while (!inputStack.isEmpty()) {
            int tmp = inputStack.pop();

            while (!retValue.isEmpty() && retValue.peek() > tmp) {
                inputStack.add(retValue.pop());
            }

            retValue.add(tmp);
        }

        return retValue;
    }
}
