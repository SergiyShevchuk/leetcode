package com.company.Pramp;

import java.util.*;

public class PRM_003 {

    public static int[] sortKMessedArray(int[] arr, int k) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            priorityQueue.add(arr[i]);
            if (priorityQueue.size() > k) {
                arr[counter++] = priorityQueue.poll();
            }
        }

        for (int i = 0; i < k; i++) {
            arr[counter++] = priorityQueue.poll();
        }

        return arr;
    }


    // input:  arr = [1, 4, 5, 2, 3, 7, 8, 6, 10, 9], k = 2
    //
    // output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


    public static int indexEqualsValueSearch(int[] arr) {

        int left = 0; // 2
        int right = arr.length - 1;

        while(left <= right){
            int midd = left + (right - left) / 2; // 2 + (3 - 2) / 2
            if (arr[midd] == midd ) { // 0 == 1
                return midd;
            } else if (arr[midd] > midd) { // 0 < 1
                left = midd + 1;
            } else {
                right = midd - 1;
            }
        }

        return -1;
    }

//    public static int[] sortKMessedArray(int[] arr, int k) {
//
//        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
//
//        for (int i = 0; i <= k; i++) {
//            priorityQueue.add(arr[i]);
//        }
//
//        for (int i = k + 1; i < arr.length - 1; i++) {
//            arr[i-(k+1)] = priorityQueue.poll();
//            priorityQueue.add(arr[i]);
//        }
//
//        for (int i = 0; i <= k; i++) {
//            arr[arr.length - k - 1 + i] = priorityQueue.poll();
//        }
//
//        return arr;
//    }
}
