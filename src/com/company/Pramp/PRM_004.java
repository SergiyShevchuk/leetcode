package com.company.Pramp;

public class PRM_004 {
    static int[] arrayOfArrayProducts(int[] arr) {

        if (arr.length == 0 || arr.length == 1) {
            return arr;
        }

        int[] leftPart = new int[arr.length];
        int[] rightPart = new int[arr.length];

        // [2, 7, 3, 4]

        leftPart[0] = 1;  // 1 2 14 42
        rightPart[rightPart.length - 1] = 1; // 0 0 0 1
        for (int i = 1; i < leftPart.length; i++) {
            leftPart[i] = leftPart[i - 1] * arr[i - 1];
        }
        for (int i = rightPart.length - 2; i >= 0; i--) {
            rightPart[i] = rightPart[i + 1] * arr[i + 1];
        }

        for (int i = 0; i < arr.length; i++) {
            arr[i] = rightPart[i] * leftPart[i];
        }

        return arr;
    }
}
