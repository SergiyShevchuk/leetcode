package com.company.Pramp;

/*

K-Messed Array Sort
Given an array of integers arr where each element is at most k places
away from its sorted position,
code an efficient function sortKMessedArray that sorts arr.
For instance, for an input array of size 10 and k = 2, an element
belonging to index 6 in the sorted array will be located at
either index 4, 5, 6, 7 or 8 in the input array.

Analyze the time and space complexities of your solution.

Example:
    input:  arr = [1, 4, 5, 2, 3, 7, 8, 6, 10, 9], k = 2
    output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 */

import java.util.ArrayList;
import java.util.List;

public class PRM_002 {

    public static int[] moveZerosToEnd(int[] arr) {
        // your code goes here

    /*

    - two pointer loop:
        - i,j => i start with 0, j start with 0
        - itrate the array,swap when the item is zero

                     j
                     i
    [1, 10, 0, 2, 8, 3, 0, 0, 6, 4, 0, 5, 7, 0]
    [1, 10, 2, 8, 0, 3, 0, 0, 6, 4, 0, 5, 7, 0]
    */

        int i = 0, j =0;

        while(i < arr.length && j < arr.length){

            if(arr[j]!=0)
            {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;

                j++;
                i++;

            } else {
                j++;
            }}

        return arr;
    }

//    static int[] moveZeroesToRight(int[] arr) {
//        if (arr.length <= 1) {
//            return arr;
//        }
//
//        int pointer = 0;
//
//        for (int i = 0; i < arr.length; i++) {
//            if (arr[i] != 0) {
//                arr[pointer] = arr[i];
//                pointer++;
//            }
//        }
//
//        for (int i = pointer; i < arr.length; i++) {
//            arr[i] = 0;
//        }
//
//        return arr;
//    }
}
